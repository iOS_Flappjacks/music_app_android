package com.musicapp.musicplayer.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;

import static com.musicapp.musicplayer.core.utilities.AppConstants.BASE_URL;
import static com.musicapp.musicplayer.core.utilities.AppConstants.DHYANA;
import static com.musicapp.musicplayer.core.utilities.AppConstants.MINDFUL_MOVEMENT;
import static com.musicapp.musicplayer.core.utilities.AppConstants.PRANA;

/**
 * This NetworkService class holds the Base url and other urls used in the application.
 * All the requests to servers will be made from the NetworkService class
 * We are using Volley library to make API calls.
 *
 * @author Akhil Aravind
 */
public class NetworkService {

    OkHttpClient client = new OkHttpClient();
    Context context = null;
    Handler mainHandler = null;
    private NetworkServiceListener listener;
    Boolean cancelFlag =  false;
    private static int progressValue;
    private String LOG_TAG = "Network_Service";

    public void inject(Context context) {
        this.context = context;
        mainHandler = new Handler(context.getMainLooper());
    }

    /**
     * This method is to download the track which requested
     *
     * @param mediaUrl  the url to be downloaded
     * @param listener  this will handle the Success or failure result.
     */
    public void sendMediaDownloadRequest(String mediaUrl, final NetworkServiceListener listener) {
        this.listener = listener;
        new DownloadFileAsync(mediaUrl, context).execute();
    }

//    public void sendFetchAudioRequest(final NetworkServiceListener listener) {
//        String URL = BASE_URL + MINDFUL_MOVEMENT;
//
//        OkHttpClient client = new OkHttpClient();
//
//        Request request = new Request.Builder()
//                .url(URL)
//                .get()
//                .build();
//
//        client.newCall(request).enqueue(new Callback() {
//
//            @Override
//            public void onFailure(Call call, IOException e) {
//                mainHandler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onFailure("FAIL");
//                    }
//                });
//            }
//
//            @Override
//            public void onResponse(Call call, final Response response) throws IOException {
//
//                final String epResponse = response.body().string();
//                Log.e(LOG_TAG, epResponse);
//                Log.i(LOG_TAG, response.toString());
//
//                if (response.isSuccessful()) {
//                    mainHandler.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            try {
//                                listener.onSuccess(epResponse, false);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    });
//                } else {
//                    mainHandler.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            listener.onFailure(epResponse);
//                        }
//                    });
//                }
//            }
//
//        });
//    }

//    public void sendFetchDhyanaRequest(final NetworkServiceListener listener) {
//        String URL = BASE_URL + DHYANA;
//
//        OkHttpClient client = new OkHttpClient();
//
//        Request request = new Request.Builder()
//                .url(URL)
//                .get()
//                .build();
//
//        client.newCall(request).enqueue(new Callback() {
//
//            @Override
//            public void onFailure(Call call, IOException e) {
//                mainHandler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onFailure("FAIL");
//                    }
//                });
//            }
//
//            @Override
//            public void onResponse(Call call, final Response response) throws IOException {
//
//                final String epResponse = response.body().string();
//                Log.e(LOG_TAG, epResponse);
//                Log.i(LOG_TAG, response.toString());
//
//                if (response.isSuccessful()) {
//                    mainHandler.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            try {
//                                listener.onSuccess(epResponse, false);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    });
//                } else {
//                    mainHandler.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            listener.onFailure(epResponse);
//                        }
//                    });
//                }
//            }
//
//        });
//    }
//
//    public void sendFetchPranaRequest(final NetworkServiceListener listener) {
//        String URL = BASE_URL + PRANA;
//
//        OkHttpClient client = new OkHttpClient();
//
//        Request request = new Request.Builder()
//                .url(URL)
//                .get()
//                .build();
//
//        client.newCall(request).enqueue(new Callback() {
//
//            @Override
//            public void onFailure(Call call, IOException e) {
//                mainHandler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onFailure("FAIL");
//                    }
//                });
//            }
//
//            @Override
//            public void onResponse(Call call, final Response response) throws IOException {
//
//                final String epResponse = response.body().string();
//                Log.e(LOG_TAG, epResponse);
//                Log.i(LOG_TAG, response.toString());
//
//                if (response.isSuccessful()) {
//                    mainHandler.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            try {
//                                listener.onSuccess(epResponse, false);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    });
//                } else {
//                    mainHandler.post(new Runnable() {
//                        public void run() {
//                            listener.onFailure(epResponse);
//                        }
//                    });
//                }
//            }
//
//        });
//    }

    /**
     * Interface to handle success and failure response
     */
    public interface NetworkServiceListener {
        void onFailure(String response);

        void onSuccess(String response, Boolean cancelFlag);
    }

    /**
     * This method will check the network connection
     *
     * @return true or false
     */
    public boolean haveNetworkAccess() {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager != null) {
            NetworkInfo connection = manager.getActiveNetworkInfo();
            if (connection != null && connection.isConnectedOrConnecting()) {
                return true;
            }
        }
        return false;
    }

    /**
     * The AsyncTask which downloads the track in a background thread
     */
    class DownloadFileAsync extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialogDownload;
        Context context;
        private File f = null;
        String fileUrl, fileNameUrl;

        DownloadFileAsync( String fileUrl, Context context) {
            this.fileUrl = fileUrl;
            this.context = context;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialogDownload = new ProgressDialog(context);
            progressDialogDownload.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialogDownload.setTitle("Downloading...");
            progressDialogDownload.setMax(100);
            cancelFlag = false;
            progressDialogDownload.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    cancelFlag = true;
                    cancel(true);
                    dialog.dismiss();
                }
            });
            progressDialogDownload.setCancelable(true);
            progressDialogDownload.setCanceledOnTouchOutside(false);
            progressDialogDownload.show();

        }

        @Override
        protected String doInBackground(String... aurl) {

            try{

                ContextWrapper contextWrapper = new ContextWrapper(context);
                File directory = contextWrapper.getDir(context.getFilesDir().getName(), Context.MODE_PRIVATE);

                File cacheDir=new File(directory,"MusicPlayerDemo");
                if(!cacheDir.exists())
                    cacheDir.mkdirs();

                URI uri = null;

                try {
                    uri = new URI(fileUrl);
                    URL videoUrl = uri.toURL();
                    File tempFile = new File(videoUrl.getFile());
                    fileNameUrl = tempFile.getName();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }

                f=new File(cacheDir,fileNameUrl);

                URL url = new URL(fileUrl);

                URLConnection conexion = url.openConnection();
                conexion.connect();
                int lengthOfFile = conexion.getContentLength();

                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(f);

                byte data[] = new byte[1024];
                long total = 0;
                int count = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    progressValue = (int)((total*100)/lengthOfFile);
                    publishProgress(String.valueOf(progressValue));
                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();

            }
            catch(Exception e){
                Log.e("Download_Failed", "Please check your internet connection");
                e.printStackTrace();
                cancel(true);
                listener.onFailure("Download Failed. Please check your internet connection");
            }
            return f.getAbsolutePath();
        }

        protected void onProgressUpdate(String... progress) {
            progressDialogDownload.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(String path) {
            progressDialogDownload.dismiss();
            listener.onSuccess(path, cancelFlag);
        }
    }

    /**
     * This method will fetch the Dhwani tracks
     *
     * @param listener this will handle the Success or failure result.
     */
    public void sendFetchAudioRequest(final NetworkServiceListener listener) {
        long unixTime = System.currentTimeMillis() / 1000L;

        String URL = BASE_URL + MINDFUL_MOVEMENT + "/" + unixTime;

        Log.e("sendFetchAudioRequest", URL);

        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest sr = new StringRequest(com.android.volley.Request.Method.GET, URL,

                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(LOG_TAG, response);
                        listener.onSuccess(response, false);
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VolleyMyRides", "error: " + error.toString());
                        listener.onFailure(error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put(getResources().getString(R.string.authorization),API_TOKEN);
                return params;

            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.getCache().clear();
        queue.add(sr);
    }

    /**
     * This method will fetch the Dhyana tracks
     *
     * @param listener this will handle the Success or failure result.
     */
    public void sendFetchDhyanaRequest(final NetworkServiceListener listener) {

        long unixTime = System.currentTimeMillis() / 1000L;
        String URL = BASE_URL + DHYANA + "/" + unixTime;

        Log.e("sendFetchDhyanaRequest", URL);

        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest sr = new StringRequest(com.android.volley.Request.Method.GET, URL,

                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(LOG_TAG, response);
                        listener.onSuccess(response, false);
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VolleyMyRides", "error: " + error.toString());
                        listener.onFailure(error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put(getResources().getString(R.string.authorization),API_TOKEN);
                return params;

            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.getCache().clear();
        queue.add(sr);
    }

    /**
     * This method will fetch the Parana tracks
     *
     * @param listener this will handle the Success or failure result.
     */
    public void sendFetchPranaRequest(final NetworkServiceListener listener) {
        long unixTime = System.currentTimeMillis() / 1000L;

        String URL = BASE_URL + PRANA + "/" + unixTime;

        Log.e("sendFetchPranaRequest", URL);

        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest sr = new StringRequest(com.android.volley.Request.Method.GET, URL,

                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(LOG_TAG, response);
                        listener.onSuccess(response, false);
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VolleyMyRides", "error: " + error.toString());
                        listener.onFailure(error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put(getResources().getString(R.string.authorization),API_TOKEN);
                return params;

            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.getCache().clear();
        queue.add(sr);
    }
}