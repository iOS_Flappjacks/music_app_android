package com.musicapp.musicplayer.core.ui.splashFragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.musicapp.musicplayer.R;
import com.musicapp.musicplayer.core.ui.homeFragment.HomeFragment;
import com.musicapp.musicplayer.core.ui.mainActivity.BaseFragment;
import com.musicapp.musicplayer.services.StorageService;

import static com.musicapp.musicplayer.core.ui.mainActivity.MainActivity.haveNetworkAccess;
import static com.musicapp.musicplayer.core.ui.mainActivity.MainActivity.sansRegular;
import static com.musicapp.musicplayer.core.ui.mainActivity.MainActivity.sansSemiBold;
import static com.musicapp.musicplayer.core.ui.mainActivity.MainActivity.sanslight;
import static com.musicapp.musicplayer.core.utilities.DialogManager.showInternetDialog;

/**
 * This fragment is used to display the splash screen.
 *
 * @author Akhil Aravind
 */
public class SplashFragment extends BaseFragment implements View.OnClickListener{

    /**
     * Initializing the variables
     */
    private View rootView;
    private Button btnClose, btnHome;
    TextView textView1, textView2, textView3;
    StorageService storageService;
    Handler handler;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_splash_screen, container, false);
        btnClose = rootView.findViewById(R.id.btn_close_fss);
        btnHome = rootView.findViewById(R.id.btn_home_fss);
        textView1 = rootView.findViewById(R.id.textview1_fss);
        textView2 = rootView.findViewById(R.id.textview2_fss);
        textView3 = rootView.findViewById(R.id.textview3_fss);

        storageService = new StorageService(mActivity);
        handler = new Handler();
        btnClose.setOnClickListener(this);
        btnHome.setOnClickListener(this);

        /**
         * Setting the fonts
         */
        textView1.setTypeface(sanslight);
        textView2.setTypeface(sansRegular);
        textView3.setTypeface(sansSemiBold);

        return rootView;
    }

    /**
     * This method is used to handle the click events
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_home_fss:
                if (storageService.getAudioTracks().equals("") || storageService.getDhyanaTracks().equals("") ||
                        storageService.getPranaTracks().equals("")) {
                    if (!haveNetworkAccess(mActivity)) {
                        showInternetDialog(mActivity);
                    } else {
                        Toast.makeText(mActivity, "Loading tracks... Please wait", Toast.LENGTH_LONG).show();
                        handler.post(runnable);
                        btnHome.setEnabled(false);
                    }
                } else {
                    setFragmentWithoutBackstack(new HomeFragment());
                }
                break;
            case R.id.btn_close_fss:
                if (storageService.getAudioTracks().equals("") || storageService.getDhyanaTracks().equals("") ||
                        storageService.getPranaTracks().equals("")) {
                    if (!haveNetworkAccess(mActivity)) {
                        showInternetDialog(mActivity);
                    } else {
                        Toast.makeText(mActivity, "Loading tracks... Please wait", Toast.LENGTH_LONG).show();
                        handler.post(runnable);
                        btnClose.setEnabled(false);
                    }
                } else {
                    setFragmentWithoutBackstack(new HomeFragment());
                }
                break;
        }
    }

    /**
     * This method is used to navigate to the home fragment when ever the tracks are fetched from the API's.
     */
    Runnable runnable = new Runnable() {

        boolean isNeeded = true;
        @Override
        public void run() {
            if (isNeeded) {
                try {
                    if (storageService.getAudioTracks().equals("") || storageService.getDhyanaTracks().equals("") ||
                            storageService.getPranaTracks().equals("")) {

                    } else {
                        setFragmentWithoutBackstack(new HomeFragment());
                        isNeeded = false;
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                } finally {
                    //also call the same runnable to call it at regular interval
                    handler.postDelayed(this, 1000);
                }
            } else {
                handler.removeCallbacks(runnable);
            }
        }
    };

}
