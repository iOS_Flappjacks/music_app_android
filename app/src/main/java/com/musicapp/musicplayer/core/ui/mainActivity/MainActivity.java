package com.musicapp.musicplayer.core.ui.mainActivity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.musicapp.musicplayer.R;
import com.musicapp.musicplayer.core.models.TrackListModel;
import com.musicapp.musicplayer.core.ui.homeFragment.HomeFragment;
import com.musicapp.musicplayer.core.ui.nowPlayingFragment.NowPlayingFragment;
import com.musicapp.musicplayer.core.ui.splashFragment.SplashFragment;
import com.musicapp.musicplayer.core.ui.videoPlayFragment.VideoPlayingFragment;
import com.musicapp.musicplayer.services.NetworkService;
import com.musicapp.musicplayer.services.StorageService;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cn.jzvd.Jzvd;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import static com.musicapp.musicplayer.core.utilities.DialogManager.showInternetDialog;

/**
 * This Activity is the backbone of this app.
 * The main activity which contains the fragment container.
 *
 * @author Akhil Aravind
 */
public class MainActivity extends AppCompatActivity implements MainActivityView {


    /**
     * Declaring the variables
     */
    private android.support.v4.app.FragmentManager fm;
    StorageService storageService;
    public static Typeface sanslight, sansRegular, sansExtraBold, sansSemiBold;
    private BroadcastReceiver mBatInfoReceiver = null;
    private boolean showBattery = true;
    private MainActivityPresenterImpl presenter;
    private List<TrackListModel> audioTrackList = new ArrayList<>();
    private List<TrackListModel> dhyanaTrackList = new ArrayList<>();
    private List<TrackListModel> pranaTrackList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        storageService = new StorageService(this);

        presenter = new MainActivityPresenterImpl(this);
        NetworkService networkService = new NetworkService();
        networkService.inject(MainActivity.this);
        presenter.inject(networkService, new StorageService(MainActivity.this));

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        /**
         * Setting the fonts on the screen.
         */
        sanslight = Typeface.createFromAsset(this.getAssets(), "fonts/OpenSans-Light.ttf");
        sansRegular = Typeface.createFromAsset(this.getAssets(), "fonts/OpenSans-Regular.ttf");
        sansExtraBold = Typeface.createFromAsset(this.getAssets(), "fonts/OpenSans-ExtraBold.ttf");
        sansSemiBold = Typeface.createFromAsset(this.getAssets(), "fonts/OpenSans-Semibold.ttf");

        setFragmentWithoutBackstack(new SplashFragment());

//        this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

//        try {
//
//            @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
//
//                @Override
//                protected void onPreExecute() {
//
//                }
//
//                @Override
//                protected Void doInBackground(Void... arg0) {
//                    try {
//                        //Do something...
//                        //Thread.sleep(5000);
//                        for (int i = 0; i<4; i++) {
//                            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//                            retriever.setDataSource(mediaList.get(i), new HashMap<String, String>());
//                            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
//                            long timeInMillisec = Long.parseLong(time);
//                            retriever.release();
//
//                            Gson gson = new Gson();
//                            String jsonTracks = gson.toJson(mediaList);
//                            storageService.storeVideoTracksDuration(jsonTracks);
//                        }
//                    } catch (Exception e) {
//                        // TODO Auto-generated catch block
//                        e.printStackTrace();
//                    }
//                    return null;
//                }
//
//                @Override
//                protected void onPostExecute(Void result) {
//                }
//            };
//            task.execute((Void[]) null);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        mBatInfoReceiver = new BroadcastReceiver(){
            @Override
            public void onReceive(Context ctxt, Intent intent) {
                int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);

                if (level <= 20) {
                    if (showBattery) {
                        showBatteryDialog(MainActivity.this);
                        showBattery = false;
                    }
                }
            }
        };
    }

    /**
     * This method is basic onPause method which will handle the minimize events of views
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (mBatInfoReceiver != null) {
            unregisterReceiver(mBatInfoReceiver);
        }
    }

    /**
     * This method is basic onResume method which will handle the maximize or starting events of views
     */
    @Override
    protected void onResume() {
        super.onResume();
        presenter.fetchAudioTracks();
        this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    /**
     * This method will handle the all back key press
     */
    @Override
    public void onBackPressed() {
        if (Jzvd.backPress()) {
            return;
        }
        Fragment fragment = doGetVisibleFragment();

        if (fragment instanceof HomeFragment) {
            this.finishAffinity();
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {

                getSupportFragmentManager().popBackStackImmediate();

            } else {
                this.finishAffinity();
            }
        }
    }

    /**
     * This method will give the current visible fragment
     */
    private Fragment doGetVisibleFragment() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        @SuppressLint("RestrictedApi") List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }

    /**
     * This method will call when the view is destroyed
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
//        if (mBatInfoReceiver != null) {
//            unregisterReceiver(mBatInfoReceiver);
//        }
    }

    /**
     * This method will set the fragment without adding it to the backstack
     */
    public void setFragmentWithoutBackstack(Fragment frag) {

        if (frag != null) {
            fm = MainActivity.this.getSupportFragmentManager();

            try {
                fm.beginTransaction()
                        .replace(R.id.fl_container_main, frag)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            // error in creating fragment
            Log.e("fragment", "Error in creating fragment");
        }
    }

    /**
     * This method will check the network connection
     *
     * @return true or false
     */
    public static boolean haveNetworkAccess(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager != null) {
            NetworkInfo connection = manager.getActiveNetworkInfo();
            if (connection != null && connection.isConnectedOrConnecting()) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method will check the battery percentage
     */
    public static int getBatteryPercentage(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        float batteryPct = level / (float)scale;
        float p = batteryPct * 100;

        return (int)p;
    }

    /**
     * This method will display the battery low dialog
     */
    void showBatteryDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Battery Low! Turn off the player to save your battery")
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
//                .setNegativeButton("Quit", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.dismiss();
//                    }
//                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * This method will save the downloaded tracks to preferences
     */
    @Override
    public void saveMediaPath(int position, String mediaPath) {

    }

    /**
     * This method will show the progress dialogs
     */
    @Override
    public void showProgressDialog() {

    }

    /**
     * This method will hide the progress dialogs
     */
    @Override
    public void hideProgressDialog() {

    }

    /**
     * This method will show the toasts
     */
    @Override
    public void showMessage(String message) {
        if (message.contains("connection")) {
            Fragment fragment = doGetVisibleFragment();

            if (fragment instanceof VideoPlayingFragment || fragment instanceof NowPlayingFragment) {

            } else {
                showInternetDialog(MainActivity.this);
            }
        } else {
            Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This method will handle the creation and update of downloaded tracks for Dhwani
     * If not present, it will save. Else it will update
     * This will check and replace the tracks for which the UUID's not matching
     */
    @Override
    public void onFetchAudioDataReceived(List<TrackListModel> myTrackList) {
        Log.e("onFetchAudioData", String.valueOf(myTrackList.size()));
        presenter.fetchDhyanaTracks();

        if (myTrackList.size() != 0) {
            if (!storageService.getAudioTracks().equalsIgnoreCase("")) {
                Log.e("AudioData", " - Exists");

                Gson gson = new Gson();
                String jsonTracks = storageService.getAudioTracks();
                Type type = new TypeToken<ArrayList<TrackListModel>>() {
                }.getType();
                audioTrackList = gson.fromJson(jsonTracks, type);

                if (myTrackList.size() == audioTrackList.size()) {
                    Log.e("AudioData", "- case1");

                    for (int i = 0; i < audioTrackList.size(); i++) {

                        if (myTrackList.get(i).getTrack_id() == audioTrackList.get(i).getTrack_id()) {
                            if (!myTrackList.get(i).getUuid().equals(audioTrackList.get(i).getUuid())) {
                                Log.e("AudioData", "- Object changed at position - " + i);
                                audioTrackList.set(i, myTrackList.get(i));
                            }
                        } else {
                            Log.e("AudioData", "- Object changed at position - " + i);
                            audioTrackList.set(i, myTrackList.get(i));
                        }
                    }
                } else if (myTrackList.size() > audioTrackList.size()) {
                    Log.e("AudioData", "- case2");

                    for (int i = 0; i < audioTrackList.size(); i++) {

                        if (myTrackList.get(i).getTrack_id() == audioTrackList.get(i).getTrack_id()) {
                            if (!myTrackList.get(i).getUuid().equals(audioTrackList.get(i).getUuid())) {
                                Log.e("AudioData", "- Object changed at position - " + i);
                                audioTrackList.set(i, myTrackList.get(i));
                            }
                        } else {
                            Log.e("AudioData", "- Object changed at position - " + i);
                            audioTrackList.set(i, myTrackList.get(i));
                        }
                    }

                    for (int i = audioTrackList.size(); i < myTrackList.size(); i++) {
                        audioTrackList.add(myTrackList.get(i));
                    }

                    Log.e("audioTrackList", String.valueOf(audioTrackList.size()));

                } else if (myTrackList.size() < audioTrackList.size()) {
                    Log.e("AudioData", "- case3");

                    for (int i = 0; i < myTrackList.size(); i++) {
                        if (myTrackList.get(i).getTrack_id() == audioTrackList.get(i).getTrack_id()) {
                            if (!myTrackList.get(i).getUuid().equals(audioTrackList.get(i).getUuid())) {
                                Log.e("AudioData", "- Object changed at position - " + i);
                                audioTrackList.set(i, myTrackList.get(i));
                            }
                        } else {
                            Log.e("AudioData", "- Object changed at position - " + i);
                            audioTrackList.set(i, myTrackList.get(i));
                        }
                    }

                    for (int i = myTrackList.size() - 1; i < audioTrackList.size(); i++) {
                        audioTrackList.remove(i);
                    }
                    Log.e("audioTrackList", String.valueOf(audioTrackList.size()));
                }

                String jsonTracksLocal = gson.toJson(audioTrackList);
                storageService.storeAudioTracks(jsonTracksLocal);
            } else {
                Gson gson = new Gson();
                String jsonTracks = gson.toJson(myTrackList);
                storageService.storeAudioTracks(jsonTracks);
            }
        } else {
            Gson gson = new Gson();
            String jsonTracks = gson.toJson(myTrackList);
            storageService.storeAudioTracks(jsonTracks);
        }
    }

    /**
     * This method will handle the creation and update of downloaded tracks for Dhyana
     * If not present, it will save. Else it will update
     * This will check and replace the tracks for which the UUID's not matching
     */
    @Override
    public void onFetchDhyanaDataReceived(List<TrackListModel> myTrackList) {
        Log.e("onFetchDhyanaData", String.valueOf(myTrackList.size()));
        presenter.fetchPranaTracks();

        if (myTrackList.size() != 0) {
            if (!storageService.getDhyanaTracks().equalsIgnoreCase("")) {
                Log.e("DhyanaData", " - Exists");

                Gson gson = new Gson();
                String jsonTracks = storageService.getDhyanaTracks();
                Type type = new TypeToken<ArrayList<TrackListModel>>() {
                }.getType();
                dhyanaTrackList = gson.fromJson(jsonTracks, type);

                if (myTrackList.size() == dhyanaTrackList.size()) {
                    Log.e("DhyanaData", "- case1");

                    for (int i = 0; i < dhyanaTrackList.size(); i++) {

                        if (myTrackList.get(i).getTrack_id() == dhyanaTrackList.get(i).getTrack_id()) {
                            if (!myTrackList.get(i).getUuid().equals(dhyanaTrackList.get(i).getUuid())) {
                                Log.e("DhyanaData", "- Object changed at position - " + i);
                                dhyanaTrackList.set(i, myTrackList.get(i));
                            }
                        } else {
                            Log.e("DhyanaData", "- Object changed at position - " + i);
                            dhyanaTrackList.set(i, myTrackList.get(i));
                        }
                    }
                } else if (myTrackList.size() > dhyanaTrackList.size()) {
                    Log.e("DhyanaData", "- case2");

                    for (int i = 0; i < dhyanaTrackList.size(); i++) {

                        if (myTrackList.get(i).getTrack_id() == dhyanaTrackList.get(i).getTrack_id()) {
                            if (!myTrackList.get(i).getUuid().equals(dhyanaTrackList.get(i).getUuid())) {
                                Log.e("DhyanaData", "- Object changed at position - " + i);
                                dhyanaTrackList.set(i, myTrackList.get(i));
                            }
                        } else {
                            Log.e("DhyanaData", "- Object changed at position - " + i);
                            dhyanaTrackList.set(i, myTrackList.get(i));
                        }
                    }

                    for (int i = dhyanaTrackList.size(); i < myTrackList.size(); i++) {
                        dhyanaTrackList.add(myTrackList.get(i));
                    }

                    Log.e("dhyanaTrackList", String.valueOf(dhyanaTrackList.size()));

                } else if (myTrackList.size() < dhyanaTrackList.size()) {
                    Log.e("DhyanaData", "- case3");

                    for (int i = 0; i < myTrackList.size(); i++) {
                        if (myTrackList.get(i).getTrack_id() == dhyanaTrackList.get(i).getTrack_id()) {
                            if (!myTrackList.get(i).getUuid().equals(dhyanaTrackList.get(i).getUuid())) {
                                Log.e("DhyanaData", "- Object changed at position - " + i);
                                dhyanaTrackList.set(i, myTrackList.get(i));
                            }
                        } else {
                            Log.e("DhyanaData", "- Object changed at position - " + i);
                            dhyanaTrackList.set(i, myTrackList.get(i));
                        }
                    }

                    for (int i = myTrackList.size() - 1; i < dhyanaTrackList.size(); i++) {
                        dhyanaTrackList.remove(i);
                    }
                    Log.e("dhyanaTrackList", String.valueOf(dhyanaTrackList.size()));
                }

                String jsonTracksLocal = gson.toJson(dhyanaTrackList);
                storageService.storeDhyanaTracks(jsonTracksLocal);
            } else {
                Gson gson = new Gson();
                String jsonTracks = gson.toJson(myTrackList);
                storageService.storeDhyanaTracks(jsonTracks);
            }
        } else {
            Gson gson = new Gson();
            String jsonTracks = gson.toJson(myTrackList);
            storageService.storeDhyanaTracks(jsonTracks);
        }
    }

    /**
     * This method will handle the creation and update of downloaded tracks for Parana
     * If not present, it will save. Else it will update
     * This will check and replace the tracks for which the UUID's or trackId's not matching
     */
    @Override
    public void onFetchPranaDataReceived(List<TrackListModel> myTrackList) {
        Log.e("onFetchPranaData", String.valueOf(myTrackList.size()));

        if (myTrackList.size() != 0) {
            if (!storageService.getPranaTracks().equalsIgnoreCase("")) {
                Log.e("PranaData", " - Exists");

                Gson gson = new Gson();
                String jsonTracks = storageService.getPranaTracks();
                Type type = new TypeToken<ArrayList<TrackListModel>>() {
                }.getType();
                pranaTrackList = gson.fromJson(jsonTracks, type);

                if (myTrackList.size() == pranaTrackList.size()) {
                    Log.e("PranaData", "- case1");

                    for (int i = 0; i < pranaTrackList.size(); i++) {

                        if (myTrackList.get(i).getTrack_id() == pranaTrackList.get(i).getTrack_id()) {
                            if (!myTrackList.get(i).getUuid().equals(pranaTrackList.get(i).getUuid())) {
                                Log.e("PranaData", "- Object changed at position - " + i);
                                pranaTrackList.set(i, myTrackList.get(i));
                            }
                        } else {
                            Log.e("PranaData", "- Object changed at position - " + i);
                            pranaTrackList.set(i, myTrackList.get(i));
                        }
                    }
                } else if (myTrackList.size() > pranaTrackList.size()) {
                    Log.e("PranaData", "- case2");

                    for (int i = 0; i < pranaTrackList.size(); i++) {

                        if (myTrackList.get(i).getTrack_id() == pranaTrackList.get(i).getTrack_id()) {
                            if (!myTrackList.get(i).getUuid().equals(pranaTrackList.get(i).getUuid())) {
                                Log.e("PranaData", "- Object changed at position - " + i);
                                pranaTrackList.set(i, myTrackList.get(i));
                            }
                        } else {
                            Log.e("PranaData", "- Object changed at position - " + i);
                            pranaTrackList.set(i, myTrackList.get(i));
                        }
                    }

                    for (int i = pranaTrackList.size(); i < myTrackList.size(); i++) {
                        pranaTrackList.add(myTrackList.get(i));
                    }

                    Log.e("pranaTrackList", String.valueOf(pranaTrackList.size()));

                } else if (myTrackList.size() < pranaTrackList.size()) {
                    Log.e("PranaData", "- case3");

                    for (int i = 0; i < myTrackList.size(); i++) {
                        if (myTrackList.get(i).getTrack_id() == pranaTrackList.get(i).getTrack_id()) {
                            if (!myTrackList.get(i).getUuid().equals(pranaTrackList.get(i).getUuid())) {
                                Log.e("PranaData", "- Object changed at position - " + i);
                                pranaTrackList.set(i, myTrackList.get(i));
                            }
                        } else {
                            Log.e("PranaData", "- Object changed at position - " + i);
                            pranaTrackList.set(i, myTrackList.get(i));
                        }
                    }

                    for (int i = myTrackList.size() - 1; i < pranaTrackList.size(); i++) {
                        pranaTrackList.remove(i);
                    }
                    Log.e("pranaTrackList", String.valueOf(pranaTrackList.size()));
                }

                String jsonTracksLocal = gson.toJson(pranaTrackList);
                storageService.storePranaTracks(jsonTracksLocal);

            } else {
                Gson gson = new Gson();
                String jsonTracks = gson.toJson(myTrackList);
                storageService.storePranaTracks(jsonTracks);
            }
        } else {
            Gson gson = new Gson();
            String jsonTracks = gson.toJson(myTrackList);
            storageService.storePranaTracks(jsonTracks);
        }
    }
}
