package com.musicapp.musicplayer.core.ui.nowPlayingFragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.musicapp.musicplayer.R;
import com.musicapp.musicplayer.core.models.TrackListModel;
import com.musicapp.musicplayer.core.ui.homeFragment.HomeFragment;
import com.musicapp.musicplayer.core.ui.mainActivity.BaseFragment;
import com.musicapp.musicplayer.core.utilities.DialogManager;
import com.musicapp.musicplayer.services.NetworkService;
import com.musicapp.musicplayer.services.StorageService;
import com.willy.ratingbar.BaseRatingBar;
import com.willy.ratingbar.ScaleRatingBar;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static android.view.View.GONE;
import static com.musicapp.musicplayer.core.ui.mainActivity.MainActivity.haveNetworkAccess;
import static com.musicapp.musicplayer.core.ui.mainActivity.MainActivity.sansExtraBold;
import static com.musicapp.musicplayer.core.ui.mainActivity.MainActivity.sansRegular;
import static com.musicapp.musicplayer.core.ui.mainActivity.MainActivity.sanslight;

/**
 * This fragment is used to display the Audio Player screen
 *
 * @author Akhil Aravind
 */
public class NowPlayingFragment extends BaseFragment implements RecyclerViewClickInterface, NowPlayingFragmentView, View.OnClickListener {

    /**
     * Declaring the variables
     */
    private Button btnPrevious, btnNext, btnPlay, btnPause;
    LinearLayout llSwipe, llPlaylist;
    RelativeLayout rlPlaylist;
    private MediaPlayer mediaPlayer;
    String t1 , t2, p1, p2;

    private double startTime = 0;
    private double finalTime = 0;

    private Handler myHandler = new Handler();;
    private int forwardTime = 5000;
    private int backwardTime = 5000;
    private SeekBar seekbar;
    private TextView tvPlayTime, tvSongName, tvArtist, tvHeader, tvArtistName, tvWatchPlaylist, tvPlaylist;

    public static int oneTimeOnly = 0;
    private String fileName = "sample.mp3";
    private ImageView ivDownload, ivHome;
    private RecyclerView rvMedia;
    StorageService storageService;

    List<String> mediaPathList = new ArrayList<>();
    List<String> offlineMediaImageList = new ArrayList<String>();
    List<String> mediaTitleList = new ArrayList<String>();
    List<String> artistList = new ArrayList<String>();
    List<String> ratingList = new ArrayList<String>();
    List<String> backgroundImageList = new ArrayList<String>();
    List<String> mediaPathListDownload = new ArrayList<>();

    private String mediaUrlPath = "";
    private NowPlayingAdapter nowPlayingAdapter;
    private int position = 0;
    private RelativeLayout rlParent;
    private View rootView;
    private Dialog dialogProgress;
    private LottieAnimationView lottieAnimationView;
    private NowPlayingPresenter presenter;
    private boolean autoPlay = false;
    private boolean switchNext = false;
    private ImageView gifView;
    private ProgressDialog pd;
    private FrameLayout flMain;
    private float x1,x2;
    static final int MIN_DISTANCE = 100;
    private boolean goToSettings = false;
    private List<TrackListModel> audioTrackList = new ArrayList<>();
    private TextView tvNoTracks;
    private ImageView ivRate;
    private TextView tvRating;
    private Dialog dialog;
    final Float[] rating = {0.0F};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_now_playing, container, false);

        init();
        setListeners();
        return rootView;
    }

    /**
     * This method will initialize every variables
     * Initialize the NowPlayingPresenter
     * Initialize Network service, progress dialog
     */
    private void init() {
        storageService = new StorageService(mActivity);

        presenter = new NowPlayingPresenterImpl(this);
        NetworkService networkService = new NetworkService();
        networkService.inject(mActivity);
        presenter.inject(networkService, new StorageService(mActivity));

        dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_rate);

        flMain = rootView.findViewById(R.id.fl_parent_fnp);
        llSwipe = rootView.findViewById(R.id.ll_swipe_playlist_fnp);
        llPlaylist = rootView.findViewById(R.id.ll_playlist_fnp);
        rlPlaylist = rootView.findViewById(R.id.rl_playlist_fnp);
        rlParent = rootView.findViewById(R.id.rl_parent_fnp);
        gifView = rootView.findViewById(R.id.gifview_fnp);
        btnPlay = rootView.findViewById(R.id.btn_play_fnp);
        btnPause = rootView.findViewById(R.id.btn_pause_fnp);
        btnNext = rootView.findViewById(R.id.btn_next_fnp);
        btnPrevious = rootView.findViewById(R.id.btn_previous_fnp);
        ivDownload = rootView.findViewById(R.id.iv_download_fnp);
        ivHome = rootView.findViewById(R.id.iv_home_fnp);
        ivRate = rootView.findViewById(R.id.iv_rate_fnp);
        tvRating = rootView.findViewById(R.id.tv_rating_fnp);
        rvMedia = rootView.findViewById(R.id.rv_tracks_fnp);
        tvNoTracks = rootView.findViewById(R.id.tv_no_tracks_fnp);
        tvPlayTime = rootView.findViewById(R.id.tv_track_timer_fnp);
        tvSongName = rootView.findViewById(R.id.tv_media_name_fnp);
        tvArtist = rootView.findViewById(R.id.tv_artist_fnp);
        tvHeader = rootView.findViewById(R.id.tv_header_fnp);
        tvArtistName = rootView.findViewById(R.id.tv_artist_name_fnp);
        tvWatchPlaylist = rootView.findViewById(R.id.tv_watch_playlist_fnp);
        tvPlaylist = rootView.findViewById(R.id.tv_playlist_fnp);
        seekbar = rootView.findViewById(R.id.seek_bar_fnp);
        dialogProgress = DialogManager.getProgressDialog(mActivity);
        lottieAnimationView = dialogProgress.findViewById(R.id.lottie_cdp);

        /**
         * Setting the fonts for the screen
         */

        tvHeader.setTypeface(sanslight);
        tvArtistName.setTypeface(sansRegular);
        tvSongName.setTypeface(sansExtraBold);
        tvWatchPlaylist.setTypeface(sansRegular);
        tvPlaylist.setTypeface(sansRegular);
        tvPlayTime.setTypeface(sansRegular);
        tvNoTracks.setTypeface(sansRegular);

        /**
         * Initailising the progress dialog
         */
        pd = new ProgressDialog(mActivity);
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);
        pd.setIndeterminate(true);

        flMain.setForeground(new ColorDrawable(ContextCompat.getColor(mActivity, R.color.transparentBlack)));

        LinearLayoutManager layoutManager= new LinearLayoutManager(mActivity,LinearLayoutManager.HORIZONTAL, false);
        rvMedia.setLayoutManager(layoutManager);

        /**
         * This method will get the tracks from shared preferences
         */
        if (!storageService.getAudioTracks().equalsIgnoreCase("")) {
            Gson gson = new Gson();
            String jsonTracks = storageService.getAudioTracks();
            Type type = new TypeToken<ArrayList<TrackListModel>>() {}.getType();
            audioTrackList = gson.fromJson(jsonTracks, type);

            for (int i = 0; i < audioTrackList.size(); i++) {
                mediaPathList.add(audioTrackList.get(i).getUrl());
                backgroundImageList.add(audioTrackList.get(i).getBackground_pic());
                offlineMediaImageList.add(audioTrackList.get(i).getThumbnail());
                artistList.add(audioTrackList.get(i).getAuthor());
                ratingList.add(audioTrackList.get(i).getUserRating());

                if (audioTrackList.get(i).getTitle().length() != 0) {
                    mediaTitleList.add(audioTrackList.get(i).getTitle());
                } else {
                    mediaTitleList.add(audioTrackList.get(i).getDescription());
                }
            }

            /**
             * Checking the tracks whether empty or not.
             */
            if (mediaPathList.size() != 0) {
                mediaUrlPath = mediaPathList.get(0);
                presenter.doLoadMedia(mediaPathList.get(0));
                Glide.with(this).load(backgroundImageList.get(position)).into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            rlParent.setBackground(resource);
                        }
                    }
                });

                if (ratingList.get(0).length() > 0) {
                    tvRating.setText(ratingList.get(0).substring(0, 3));
                }
            } else {
                btnPrevious.setVisibility(View.INVISIBLE);
                btnNext.setVisibility(View.INVISIBLE);
                rvMedia.setVisibility(View.GONE);
                tvNoTracks.setVisibility(View.VISIBLE);
                Toast.makeText(mActivity, "No tracks available", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mActivity, "No tracks available", Toast.LENGTH_SHORT).show();
        }

//        if (!storageService.getAudioTracks().equalsIgnoreCase("")) {
//            Gson gson = new Gson();
//            String jsonTracks = storageService.getAudioTracks();
//            Type type = new TypeToken<ArrayList<String>>() {}.getType();
//            mediaPathList = gson.fromJson(jsonTracks, type);
//
//            for (int i = 0; i < mediaPathList.size(); i++) {
//                Log.e("mediaPathList" + i, mediaPathList.get(i));
//            }
//        }

//        if (mediaPathList.size() < 4) {
//            mediaPathList.add(AppConstants.MEDIA_URL_1);
//            mediaPathList.add(AppConstants.MEDIA_URL_2);
//            mediaPathList.add(AppConstants.MEDIA_URL_3);
//            mediaPathList.add(AppConstants.MEDIA_URL_4);
//
//            Gson gson = new Gson();
//            String jsonTracks = gson.toJson(mediaPathList);
//            storageService.storeAudioTracks(jsonTracks);
//        }
//
//        offlineMediaImageList.add(R.drawable.media1);
//        offlineMediaImageList.add(R.drawable.media2);
//        offlineMediaImageList.add(R.drawable.media3);
//        offlineMediaImageList.add(R.drawable.media4);
//
//        backgroundImageList.add(R.drawable.media1);
//        backgroundImageList.add(R.drawable.media2);
//        backgroundImageList.add(R.drawable.media3);
//        backgroundImageList.add(R.drawable.media4);

//        Bundle bundle = getArguments();
//
//        if (bundle != null) {
//            position = bundle.getInt("now_playing");
//            mediaUrlPath = mediaPathList.get(bundle.getInt("now_playing"));
//            presenter.doLoadMedia(mediaPathList.get(bundle.getInt("now_playing")));
//        } else {
//            mediaUrlPath = mediaPathList.get(0);
//            presenter.doLoadMedia(mediaPathList.get(0));
//        }

//        rlParent.setBackground(ContextCompat.getDrawable(mActivity, backgroundImageList.get(0)));

        /**
         * Initialising the adapter for the song list.
         */
        Log.e("mediaPathList", mediaPathList.size() + "---");
        nowPlayingAdapter = new NowPlayingAdapter(mActivity, mediaPathList, offlineMediaImageList, this);
        rvMedia.setAdapter(nowPlayingAdapter);

    }

    /**
     * This method will set the all the listeners
     */
    @SuppressLint("ClickableViewAccessibility")
    private void setListeners() {

        btnNext.setOnClickListener(this);
        btnPause.setOnClickListener(this);
        btnPlay.setOnClickListener(this);
        btnPrevious.setOnClickListener(this);
        ivDownload.setOnClickListener(this);
        ivHome.setOnClickListener(this);
        ivRate.setOnClickListener(this);

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (fromUser) {
                    if (mediaPlayer != null) {
                        mediaPlayer.seekTo(progress);
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        /**
         * This method handles the swipe actions on the playlist
         */
        rlPlaylist.setOnTouchListener(new OnSwipeTouchListener(mActivity) {
            Animation exitLeftAnimation = AnimationUtils.loadAnimation(mActivity, R.anim.exit_to_left);
            Animation exitRightAnimation = AnimationUtils.loadAnimation(mActivity, R.anim.exit_to_right);
            Animation enterLeftAnimation = AnimationUtils.loadAnimation(mActivity, R.anim.enter_from_left);
            Animation enterRightAnimation = AnimationUtils.loadAnimation(mActivity, R.anim.enter_from_right);

            public void onSwipeBottom() {
                if (llPlaylist.getVisibility() == View.VISIBLE) {
                    llPlaylist.startAnimation(exitRightAnimation);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            llSwipe.setVisibility(View.VISIBLE);
                            llPlaylist.setVisibility(View.GONE);
                            llSwipe.startAnimation(enterLeftAnimation);
                        }
                    }, 200);
                }
            }

            public void onSwipeRight() {
                if (llSwipe.getVisibility() == View.VISIBLE) {
                    llSwipe.startAnimation(exitLeftAnimation);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            llSwipe.setVisibility(View.GONE);
                            llPlaylist.setVisibility(View.VISIBLE);
                            llPlaylist.startAnimation(enterRightAnimation);
                        }
                    }, 200);
                }
            }
        });

        if (mediaPlayer != null) {
            mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(MediaPlayer mp, int percent) {
                    if (percent > 10) {
                        pd.dismiss();
                    } else {
                        pd.show();
                    }
                }
            });
        }
    }

    /**
     * This method will update the song time and seekbar progress
     */
    private Runnable UpdateSongTime = new Runnable() {
        public void run() {
            if (mediaPlayer != null) {
                try {
                    startTime = mediaPlayer.getCurrentPosition();
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
//            tvPlayTime.setText(String.format("%d : %d",
//                    TimeUnit.MILLISECONDS.toMinutes((long) startTime),
//                    TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
//                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
//                                    toMinutes((long) startTime)))
//            );

            String p1, p2;

            p1 = String.valueOf(TimeUnit.MILLISECONDS.toMinutes((long) startTime));
            if (p1.length() == 1) {
                p1 = 0 + p1;
            }

            p2 = String.valueOf(TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) startTime)));
            if (p2.length() == 1) {
                p2 = 0 + p2;
            }

            if (startTime <= finalTime) {
                tvPlayTime.setText(String.format("%s : %s / %s : %s", p1, p2, t1, t2));
                seekbar.setProgress((int) startTime);
            }
            myHandler.postDelayed(this, 100);
        }
    };

    /**
     * This method will handle the recycler view click events
     */
    @Override
    public void recyclerviewOnClick(int position, String mediaUrl, String mediaImage) {

        if (!mediaUrl.contains("MusicPlayerDemo")) {
            if (haveNetworkAccess(mActivity)) {

                this.position = position;

                Glide.with(this).load(backgroundImageList.get(position)).into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            rlParent.setBackground(resource);
                        }
                    }
                });

//                rlParent.setBackground(ContextCompat.getDrawable(mActivity, backgroundImageList.get(position)));
                mediaUrlPath = mediaUrl;
                Log.e("recyclerviewOnClick", mediaPathList.get(position));

                if (mediaPlayer != null) {
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.stop();
                    }
                    seekbar.refreshDrawableState();
                    mediaPlayer.release();
                    btnPause.setVisibility(GONE);
                    btnPlay.setVisibility(View.VISIBLE);
                }

                presenter.doLoadMedia(mediaUrlPath);
            } else {
                showInternetDialog(getContext());
            }
        } else {

            this.position = position;
            Glide.with(this).load(backgroundImageList.get(position)).into(new SimpleTarget<Drawable>() {
                @Override
                public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        rlParent.setBackground(resource);
                    }
                }
            });
            mediaUrlPath = mediaUrl;
            Log.e("recyclerviewOnClick", mediaPathList.get(position));

            if (mediaPlayer != null) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                seekbar.refreshDrawableState();
                mediaPlayer.release();
                btnPause.setVisibility(GONE);
                btnPlay.setVisibility(View.VISIBLE);
            }

            presenter.doLoadMedia(mediaUrlPath);
        }

    }

    /**
     * This method will handle the recycler view click events for download options
     */
    @Override
    public void recyclerviewOnClickDownload(int position, String mediaUrl) {

        presenter.doDownloadMedia(position, mediaUrl);
    }

    /**
     * This method will save the downloaded file into the app preference
     */
    @Override
    public void saveMediaPath(int positionDownload, String mediaPath, Boolean cancelFlag) {

        Log.e("cancelFlag", cancelFlag + "111");
        if (!cancelFlag) {
            if (position == positionDownload) {
                mediaUrlPath = mediaPath;
            }
            mediaPathList.set(positionDownload, mediaPath);

            audioTrackList.get(positionDownload).setUrl(mediaPath);

            Gson gson = new Gson();
            String jsonTracks = gson.toJson(audioTrackList);
            storageService.storeAudioTracks(jsonTracks);

            nowPlayingAdapter.notifyDataSetChanged();
            ivDownload.setVisibility(View.INVISIBLE);
            Log.e("saveMediaPath", mediaPath);
        }
    }

    /**
     * This method will show the progress dialogs
     */
    @Override
    public void showProgressDialog() {
//        if (!dialogProgress.isShowing()) {
//            dialogProgress.show();
//            lottieAnimationView.playAnimation();
//        }
    }

    /**
     * This method will hide the progress dialogs
     */
    @Override
    public void hideProgressDialog() {
//        if (dialogProgress.isShowing()) {
//            lottieAnimationView.cancelAnimation();
//            dialogProgress.dismiss();
//        }
    }

    /**
     * This method is used to toast any messages
     */
    @Override
    public void showMessage(String message) {
        if (message.contains("connection")) {
            showInternetDialog(mActivity);
        } else {
            Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This method is used to load the tracks and getting the music player ready to play.
     * This will handle the loading events and the delay in loading
     * Assynctask is used for handling the heavy task in background thread
     */
    @Override
    public void loadMedia(final String currentPlaying) {

        if (position == 0 ) {
            if (mediaPathList.size() == 1 ) {
                btnNext.setAlpha(0f);
                btnNext.setEnabled(false);
            }
            btnPrevious.setAlpha(0f);
            btnPrevious.setEnabled(false);
        } else if (position == mediaPathList.size() - 1 ) {
            btnNext.setAlpha(0f);
            btnNext.setEnabled(false);
        } else {
            btnNext.setAlpha(0.6f);
            btnPrevious.setAlpha(0.6f);
            btnNext.setEnabled(true);
            btnPrevious.setEnabled(true);
        }

        if (!currentPlaying.contains("MusicPlayerDemo")) {

            if (haveNetworkAccess(mActivity)) {
                ivDownload.setVisibility(View.VISIBLE);

                try {

                    @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected void onPreExecute() {
                            mediaPlayer = new MediaPlayer();
                            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                @Override
                                public void onPrepared(MediaPlayer mp) {
                                    Log.e("MediaPlayer", "prepared");

                                }
                            });

                            pd.show();

                        }

                        @Override
                        protected Void doInBackground(Void... arg0) {
                            try {
                                //Do something...
                                //Thread.sleep(5000);
                                try {
                                    Uri myUri = Uri.parse(currentPlaying);
                                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                    mediaPlayer.setDataSource(mActivity, myUri);
//                                mediaPlayer.setDataSource(mediaUrlPath);
                                    mediaPlayer.prepare();
                                } catch (IOException e) {
                                }

                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void result) {
                            if (pd != null) {
                                presenter.fetchSongDetails(mediaPlayer);
                                pd.dismiss();
                                seekbar.setProgress(0);
                                if (autoPlay) {
                                    btnPause.setVisibility(View.VISIBLE);
                                    btnPlay.setVisibility(View.GONE);
                                    mediaPlayer.start();
                                }
                                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
                                        if (position + 1 <= mediaPathList.size() - 1) {
                                            if (!mediaPathList.get(position + 1).contains("MusicPlayerDemo")) {
                                                if (haveNetworkAccess(mActivity)) {
                                                    if (ratingList.get(position).equals("0.0")) {
                                                        showDialog();
                                                        switchNext = true;
                                                    } else {
                                                        presenter.doSwitchNext();
                                                    }
                                                } else {
                                                    showInternetDialog(getContext());
                                                }
                                            } else {
                                                if (ratingList.get(position).equals("0.0")) {
                                                    showDialog();
                                                    switchNext = true;
                                                }  else {
                                                    presenter.doSwitchNext();
                                                }
                                            }
                                        }
                                        if (position == mediaPathList.size() - 1) {
                                            mediaPlayer.seekTo(0);
                                            btnPause.setVisibility(GONE);
                                            btnPlay.setVisibility(View.VISIBLE);
                                            if (ratingList.get(position).equals("0.0")) {
                                                showDialog();
                                                switchNext = false;
                                            }  else {

                                            }
                                        }
                                    }
                                });
                            }
                        }
                    };
                    task.execute((Void[]) null);

                    URI uri = null;
                    try {
                        tvSongName.setText(mediaTitleList.get(position));
                        tvArtistName.setText(artistList.get(position));
                        if (ratingList.get(position).length() > 0) {
                            tvRating.setText(ratingList.get(position).substring(0, 3));
                        }
                        uri = new URI(currentPlaying);
                        URL videoUrl = uri.toURL();
                        File tempFile = new File(videoUrl.getFile());
                        fileName = tempFile.getName();
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                showInternetDialog(mActivity);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        mActivity.onBackPressed();
//                    }
//                }, 1000);
            }
        } else {

            ivDownload.setVisibility(View.INVISIBLE);

            try {

                @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected void onPreExecute() {
                        mediaPlayer = new MediaPlayer();
                        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                Log.e("MediaPlayer", "prepared");

                            }
                        });

                        pd.show();
                    }

                    @Override
                    protected Void doInBackground(Void... arg0) {
                        try {
                            //Do something...
                            //Thread.sleep(5000);
                            try
                            {
                                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                mediaPlayer.setDataSource(currentPlaying);
                                mediaPlayer.prepare();

                            }
                            catch (IOException e) {
                            }

                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        if (pd!=null) {
                            pd.dismiss();
                            presenter.fetchSongDetails(mediaPlayer);
                            seekbar.setProgress(0);
                            if (autoPlay) {
                                btnPause.setVisibility(View.VISIBLE);
                                btnPlay.setVisibility(View.GONE);
                                mediaPlayer.start();
                            }
                            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    if (position + 1 <= mediaPathList.size() - 1) {
                                        if (!mediaPathList.get(position + 1).contains("MusicPlayerDemo")) {
                                            if (haveNetworkAccess(mActivity)) {
                                                if (ratingList.get(position).equals("0.0")) {
                                                    showDialog();
                                                    switchNext = true;
                                                }  else {
                                                    presenter.doSwitchNext();
                                                }
                                            } else {
                                                showInternetDialog(getContext());
                                            }
                                        } else {
                                            if (ratingList.get(position).equals("0.0")) {
                                                showDialog();
                                                switchNext = true;
                                            }  else {
                                                presenter.doSwitchNext();
                                            }
                                        }
                                    }
                                    if (position == mediaPathList.size() - 1) {
                                        mediaPlayer.seekTo(0);
                                        btnPause.setVisibility(GONE);
                                        btnPlay.setVisibility(View.VISIBLE);
                                        if (ratingList.get(position).equals("0.0")) {
                                            showDialog();
                                            switchNext = false;
                                        }  else {

                                        }
                                    }
                                }
                            });
                        }
                    }
                };
                task.execute((Void[])null);

                String path = currentPlaying;
                String filename=path.substring(path.lastIndexOf("/")+1);
                tvSongName.setText(mediaTitleList.get(position));
                tvArtistName.setText(artistList.get(position));
                if (ratingList.get(position).length() > 0) {
                    tvRating.setText(ratingList.get(position).substring(0, 3));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method is used to play the track
     */
    @Override
    public void doPlayMedia() {
        if (mediaPlayer != null) {
            autoPlay = true;
            btnPause.setVisibility(View.VISIBLE);
            btnPlay.setVisibility(GONE);
            mediaPlayer.start();
            myHandler.postDelayed(UpdateSongTime, 100);
        }

    }

    /**
     * This method is used to pause the track
     */
    @Override
    public void doPauseMedia() {
        if (mediaPlayer != null) {
            autoPlay = false;
            mediaPlayer.pause();
            btnPause.setVisibility(GONE);
            btnPlay.setVisibility(View.VISIBLE);
        }
    }

    /**
     * This method is used to switch to next track
     */
    @Override
    public void doSwitchNext() {

        int temp = position + 1;
        if (temp <= mediaPathList.size() - 1) {

            position = temp;

            Glide.with(this).load(backgroundImageList.get(temp)).into(new SimpleTarget<Drawable>() {
                @Override
                public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        rlParent.setBackground(resource);
                    }
                }
            });

            mediaUrlPath = mediaPathList.get(temp);

            if (mediaPlayer != null) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                seekbar.refreshDrawableState();
                mediaPlayer.release();
            }

            btnPause.setVisibility(GONE);
            btnPlay.setVisibility(View.VISIBLE);

            presenter.doLoadMedia(mediaUrlPath);
        }
    }

    /**
     * This method is used to switch to previous track
     */
    @Override
    public void doSwitchPrevious() {

        int temp = position - 1;
        if (temp >= 0) {

            position = temp;

            Glide.with(this).load(backgroundImageList.get(temp)).into(new SimpleTarget<Drawable>() {
                @Override
                public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        rlParent.setBackground(resource);
                    }
                }
            });

            Log.e("btnPreviousClick", String.valueOf(temp));
            mediaUrlPath = mediaPathList.get(temp);

            if (mediaPlayer != null) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                seekbar.refreshDrawableState();
                mediaPlayer.release();
            }

            btnPause.setVisibility(GONE);
            btnPlay.setVisibility(View.VISIBLE);

            presenter.doLoadMedia(mediaUrlPath);
        }
    }

    /**
     * This method is used to fetch the song details from the media file
     */
    @Override
    public void fetchSongDetails(MediaPlayer mediaPlayerTemp) {

        tvArtist.setVisibility(GONE);
//        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
//
//        if (mediaUrlPath.contains("MusicPlayerDemo")) {
//            mmr.setDataSource(mActivity, Uri.parse(mediaUrlPath));
//        } else {
//            if (Build.VERSION.SDK_INT >= 14)
//                mmr.setDataSource(mediaUrlPath, new HashMap<String, String>());
//            else
//                mmr.setDataSource(mediaUrlPath);
//        }
//
//        String albumName = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
//        tvArtist.setText(albumName);

        finalTime = mediaPlayerTemp.getDuration();
        startTime = mediaPlayerTemp.getCurrentPosition();

        seekbar.setMax((int) finalTime);

        t1 = String.valueOf(TimeUnit.MILLISECONDS.toMinutes((long) finalTime));
        if (t1.length() == 1) {
            t1 = 0 + t1;
        }

        t2 = String.valueOf(TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)finalTime)));
        if (t2.length() == 1) {
            t2 = 0 + t2;
        }

        p1 = String.valueOf(TimeUnit.MILLISECONDS.toMinutes((long) startTime));
        if (p1.length() == 1) {
            p1 = 0 + p1;
        }

        p2 = String.valueOf(TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) startTime)));
        if (p2.length() == 1) {
            p2 = 0 + p2;
        }

        tvPlayTime.setText(String.format("%s : %s / %s : %s", p1, p2, t1, t2));

        seekbar.setProgress((int) startTime);
    }

    /**
     * This method handles all the onClick events
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next_fnp:
                if (!mediaPathList.get(position + 1).contains("MusicPlayerDemo")) {
                    if (haveNetworkAccess(mActivity)) {
                        presenter.doSwitchNext();
                    } else {
                        showInternetDialog(getContext());
                    }
                } else {
                    presenter.doSwitchNext();
                }
                break;
            case R.id.btn_previous_fnp:
                if (!mediaPathList.get(position - 1).contains("MusicPlayerDemo")) {
                    if (haveNetworkAccess(mActivity)) {
                        presenter.doSwitchPrevious();
                    } else {
                        showInternetDialog(getContext());
                    }
                } else {
                    presenter.doSwitchPrevious();
                }
                break;
            case R.id.btn_play_fnp:
                if (mediaPathList.size() != 0) {
                    if (!mediaPathList.get(position).contains("MusicPlayerDemo")) {
                        if (haveNetworkAccess(mActivity)) {
                            presenter.doPlayMedia();
                        } else {
                            showInternetDialog(getContext());
                        }
                    } else {
                        presenter.doPlayMedia();
                    }
                } else {
                    Toast.makeText(mActivity, "No tracks available", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_pause_fnp:
                if (!mediaPathList.get(position).contains("MusicPlayerDemo")) {
                    if (haveNetworkAccess(mActivity)) {
                        presenter.doPauseMedia();
                    } else {
                        showInternetDialog(getContext());
                    }
                } else {
                    presenter.doPauseMedia();
                }
                break;
            case R.id.iv_download_fnp:
                if (mediaPathList.size() != 0) {
                    presenter.doDownloadMedia(position, mediaUrlPath);
                } else {
                    Toast.makeText(mActivity, "No tracks available", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.iv_home_fnp:
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                }
                setFragment(new HomeFragment());
                break;
            case R.id.iv_rate_fnp:
                showDialog();
                switchNext = false;
                break;

        }
    }

    /**
     * This method is basic onPause method which will handle the minimize events of views
     */
    @Override
    public void onPause() {
        super.onPause();
//        if (mediaPlayer != null) {
//            mediaPlayer.pause();
//        }
    }

    /**
     * This method is basic onResume method which will handle the maximize or starting events of views
     */
    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume", "Called");
        if (goToSettings) {
            presenter.doLoadMedia(mediaUrlPath);
            goToSettings = false;
        }
    }

    /**
     * This method will call when the view is destroyed
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        myHandler.removeCallbacksAndMessages(null);
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
                mediaPlayer.release();
            }
        }
    }

    /**
     * This method shows the internet connection dialogue
     */
    void showInternetDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Please connect to internet")
                .setCancelable(true)
                .setPositiveButton("Connect", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                });
//                .setNegativeButton("Quit", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.dismiss();
//                    }
//                });
        AlertDialog alert = builder.create();
        alert.show();

        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                goToSettings = true;
            }
        });
    }

    /**
     * This method displays the rating dialog
     */
    public void showDialog(){
        Log.e("11111111111", "2222222222");

        ScaleRatingBar ratingBar = dialog.findViewById(R.id.simpleRatingBar_cdr);
        ImageView ivClose = dialog.findViewById(R.id.iv_close_cdr);
        TextView tvTrackTitle = dialog.findViewById(R.id.tv_track_title_cdr);
        tvTrackTitle.setTypeface(sansExtraBold);
        tvTrackTitle.setText(mediaTitleList.get(position));
        ratingBar.setOnRatingChangeListener(null);
        ratingBar.setRating(Float.parseFloat(ratingList.get(position)));
        rating[0] = Float.parseFloat(ratingList.get(position));
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                ratingBar.setOnRatingChangeListener(new BaseRatingBar.OnRatingChangeListener() {
                    @Override
                    public void onRatingChange(BaseRatingBar baseRatingBar, float v) {

                        rating[0] = v;
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Do something after 5s = 5000ms

                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            }
                        }, 1000);
                    }
                });
            }
        }, 500);


        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                tvRating.setText(String.valueOf(rating[0]).substring(0,3));

                if (!storageService.getAudioTracks().equalsIgnoreCase("")) {
                    audioTrackList.get(position).setUserRating(String.valueOf(rating[0]).substring(0,3));

                    Gson gson = new Gson();
                    String modofiedTracks = gson.toJson(audioTrackList);
                    storageService.storeAudioTracks(modofiedTracks);

//                                String currentTrack = storageService.getAudioTracks();
//                                audioTrackList = gson.fromJson(currentTrack, type);

                    ratingList.set(position,String.valueOf(rating[0]).substring(0,3));

                }
                if (switchNext) {
                    presenter.doSwitchNext();
                    switchNext = false;
                }
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if (!dialog.isShowing()) {
            dialog.show();
        }
    }
}
