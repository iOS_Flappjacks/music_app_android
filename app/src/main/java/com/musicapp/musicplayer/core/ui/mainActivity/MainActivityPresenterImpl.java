package com.musicapp.musicplayer.core.ui.mainActivity;

import com.musicapp.musicplayer.core.models.TrackListModel;
import com.musicapp.musicplayer.services.NetworkService;
import com.musicapp.musicplayer.services.StorageService;

import java.util.List;

/**
 * This is the presenter implementation for the mainActivity.
 *
 * @author Akhil Aravind
 */
public class MainActivityPresenterImpl implements MainActivityPresenter,
        MainActivityInteractor.FetchAudioDataListener, MainActivityInteractor.FetchDhyanaDataListener,
        MainActivityInteractor.FetchPranaDataListener {
    private MainActivityView view;
    private MainActivityInteractor interactor;
    private int position = 0;

    MainActivityPresenterImpl(MainActivityView view) {
        this.view = view;
        interactor = new MainActivityInteractorImpl();
    }

    /**
     * This method will fetch the Dhwani tracks from server
     */
    @Override
    public void fetchAudioTracks() {
        if (view != null) {
            if (interactor != null) {
                interactor.fetchAudioTracks(this);
            }
        }
    }

    /**
     * This method will fetch the Dhyana tracks from server
     */
    @Override
    public void fetchDhyanaTracks() {
        if (view != null) {
            if (interactor != null) {
                interactor.fetchDhyanaTracks(this);
            }
        }
    }

    /**
     * This method will fetch the Prana. tracks from server
     */
    @Override
    public void fetchPranaTracks() {
        if (view != null) {
            if (interactor != null) {
                interactor.fetchPranaTracks(this);
            }
        }
    }

    /**
     * This method will show progress dialogs.
     */
    @Override
    public void showProgressDialog() {
        if (view != null) {
            view.showProgressDialog();
        }
    }

    /**
     * This method will hide progress dialogs.
     */
    @Override
    public void hideProgressDialog() {
        if (view != null) {
            view.hideProgressDialog();
        }
    }

    /**
     * This method will initialise the network and storage services
     * @param networkService the Network service
     * @param storageService the Storage service
     */
    @Override
    public void inject(NetworkService networkService, StorageService storageService) {
        interactor.inject(networkService, storageService);
    }

    /**
     * This method will give the tracks
     * @param myTrackList the arraylist of TrackListModel
     */
    @Override
    public void onFetchAudioDataReceived(List<TrackListModel> myTrackList) {
        if (view != null) {
            view.onFetchAudioDataReceived(myTrackList);
        }
    }

    /**
     * This method will give failure message
     * @param message the reason for the failure
     */
    @Override
    public void onFetchAudioFailure(String message) {
        if (view != null) {
            view.showMessage("Please check your network connection");
        }
    }

    /**
     * This method will give the tracks
     * @param myTrackList the arraylist of TrackListModel
     */
    @Override
    public void onFetchDhyanaDataReceived(List<TrackListModel> myTrackList) {
        if (view != null) {
            view.onFetchDhyanaDataReceived(myTrackList);
        }
    }

    /**
     * This method will give failure message
     * @param message the reason for the failure
     */
    @Override
    public void onFetchDhyanaFailure(String message) {
        if (view != null) {
            view.showMessage("Please check your network connection");
        }
    }

    /**
     * This method will give the tracks
     * @param myTrackList the arraylist of TrackListModel
     */
    @Override
    public void onFetchPranaDataReceived(List<TrackListModel> myTrackList) {
        if (view != null) {
            view.onFetchPranaDataReceived(myTrackList);
        }
    }

    /**
     * This method will give failure message
     * @param message the reason for the failure
     */
    @Override
    public void onFetchPranaFailure(String message) {
        if (view != null) {
            view.showMessage("Please check your network connection");
        }
    }

    /**
     * This method will be called if there is a network error.
     */
    @Override
    public void onNetworkError() {
        if (view != null) {
            view.showMessage("Please check your network connection");
        }
    }
}
