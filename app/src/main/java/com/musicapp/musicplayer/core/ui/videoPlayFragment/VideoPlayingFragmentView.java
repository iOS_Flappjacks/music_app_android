package com.musicapp.musicplayer.core.ui.videoPlayFragment;

/**
 * This is the view interface for the videoPlayFragment.
 *
 * @author Akhil Aravind
 */
public interface VideoPlayingFragmentView {

    /**
     * This method will save the tracks to preference.
     *@param position the position of that track
     *@param mediaPath media URL
     *@param cancelFlag whether user cancelled downloading or not
     */
    void saveMediaPath(int position, String mediaPath, Boolean cancelFlag);

    /**
     * This method will show toasts.
     * @param message the message to be toasted
     */
    void showMessage(String message);

    /**
     * This method will load the url
     * @param currentPlaying the url to be loaded
     */
    void loadMedia(String currentPlaying);

}

