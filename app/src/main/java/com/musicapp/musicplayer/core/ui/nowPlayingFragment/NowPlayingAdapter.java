package com.musicapp.musicplayer.core.ui.nowPlayingFragment;

/**
 * This adapter is used to display the list of Dhwani tracks.
 *
 * @author Akhil Aravind
 */
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.musicapp.musicplayer.R;

import java.util.List;

public class NowPlayingAdapter extends RecyclerView.Adapter<NowPlayingAdapter.NowPlayingAdapterViewHolder> {

    /**
     * Initialising the local variables
     */
    private List<String> mediaList;
    private List<String> imageList;
    Context context;
    RecyclerViewClickInterface listener;

    /**
     * This is an constructor used to initialise the arraylist and the context
     *
     * @param context      This param is used to specify the context.
     * @param mediaList      This is an array list holds the list of tracks url
     * @param offlineMediaImageList   This is an array list holds the list of background images.
     * @param listener recyclerView click listener
     */
    public NowPlayingAdapter(Context context, List<String> mediaList, List<String> offlineMediaImageList, RecyclerViewClickInterface listener) {
        this.mediaList = mediaList;
        this.imageList = offlineMediaImageList;
        this.context = context;
        this.listener = listener;

    }
    @Override
    public NowPlayingAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View guideListView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_media_upcoming, parent, false);
        NowPlayingAdapterViewHolder holder = new NowPlayingAdapterViewHolder(guideListView);
        return holder;
    }

    @Override
    public void onBindViewHolder(final NowPlayingAdapterViewHolder holder, final int position) {

        Glide.with(context).load(imageList.get(position)).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

                    holder.ivUpcomingMedia.setClipToOutline(true);
                    holder.ivUpcomingMedia.setImageDrawable(resource);
//                    BitmapDrawable background = new BitmapDrawable(ImageHelper.getRoundedCornerBitmap(drawableToBitmap(resource),200));
//                    holder.ivUpcomingMedia.setImageDrawable(background);
                }
            }
        });

        if (mediaList.get(position).contains("MusicPlayerDemo")) {
            holder.ivDownloadMedia.setVisibility(View.INVISIBLE);
        }

        holder.ivUpcomingMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.recyclerviewOnClick(position, mediaList.get(position), imageList.get(position));
            }
        });

        holder.ivDownloadMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.recyclerviewOnClickDownload(position, mediaList.get(position));
            }
        });
    }

    /**
     * This method is used to show the count of the views.
     *
     * @return the size of the list.
     */
    @Override
    public int getItemCount() {
        return mediaList.size();
    }

    /**
     * ViewHolder class is used to initialise the custom adapter objects.
     */
    public class NowPlayingAdapterViewHolder extends RecyclerView.ViewHolder {
        ImageView ivUpcomingMedia, ivDownloadMedia;

        public NowPlayingAdapterViewHolder(View view) {
            super(view);
            ivUpcomingMedia = view.findViewById(R.id.iv_media_image_cmu);
            ivDownloadMedia = view.findViewById(R.id.iv_download_image_cmu);
        }
    }

    /**
     * This method is used to convert the drawable to Bitmap.
     *
     * @param drawable drawable to be converted
     * @return the bitmap of corresponding drawable.
     */
    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
}
