package com.musicapp.musicplayer.core.ui.nowPlayingFragment;

import android.media.MediaPlayer;

import com.musicapp.musicplayer.services.NetworkService;
import com.musicapp.musicplayer.services.StorageService;

/**
 * This is the presenter implementation for the nowPlayingFragment.
 *
 * @author Akhil Aravind
 */
public class NowPlayingPresenterImpl implements NowPlayingPresenter, NowPlayingInteractor.MediaDataListener {
    private NowPlayingFragmentView view;
    private NowPlayingInteractor interactor;
    private int position = 0;

    NowPlayingPresenterImpl(NowPlayingFragmentView view) {
        this.view = view;
        interactor = new NowPlayingInteractorImpl();
    }

    /**
     * This method will download the track
     * @param position the position of the current downloaded track
     * @param mediaUrl the url to be downloaded
     */
    @Override
    public void doDownloadMedia(int position, String mediaUrl) {
        this.position = position;
        if (view != null) {
            if (interactor != null) {
//                view.showProgressDialog();
                interactor.downloadMedia(mediaUrl,this);
            }
        }
    }

    /**
     * This method will load the url
     * @param currentPlaying the url to be loaded
     */
    @Override
    public void doLoadMedia(String currentPlaying) {
        if (view != null) {
//            view.showProgressDialog();
            view.loadMedia(currentPlaying);
        }
    }

    /**
     * This method will show progress dialogs.
     */
    @Override
    public void showProgressDialog() {
        if (view != null) {
            view.showProgressDialog();
        }
    }

    /**
     * This method will hide progress dialogs.
     */
    @Override
    public void hideProgressDialog() {
        if (view != null) {
            view.hideProgressDialog();
        }
    }

    /**
     * This method will switch to next track
     */
    @Override
    public void doSwitchNext() {
        if (view != null) {
            view.doSwitchNext();
        }
    }

    /**
     * This method will switch to previous track
     */
    @Override
    public void doSwitchPrevious() {
        if (view != null) {
            view.doSwitchPrevious();
        }
    }

    /**
     * This method will play the track
     */
    @Override
    public void doPlayMedia() {
        if (view != null) {
            view.doPlayMedia();
        }
    }

    /**
     * This method will pause the track
     */
    @Override
    public void doPauseMedia() {
        if (view != null) {
            view.doPauseMedia();
        }
    }

    /**
     * This method will fetch the song details
     * @param mediaPlayer The mediaplayer variable to fetch the song details
     */
    @Override
    public void fetchSongDetails(MediaPlayer mediaPlayer) {
        if (view != null) {
            view.fetchSongDetails(mediaPlayer);
        }
    }

    /**
     * This method will initialise the network and storage services
     * @param networkService the Network service
     * @param storageService the Storage service
     */
    @Override
    public void inject(NetworkService networkService, StorageService storageService) {
        interactor.inject(networkService, storageService);
    }

    /**
     * This method will give the downloaded track
     * @param mediaPath the path where the file was saved
     * @param cancelFlag Flag to check whether user have cancelled the download or not
     */
    @Override
    public void onMediaDataReceived(String mediaPath, Boolean cancelFlag) {
        if (view != null) {
            view.saveMediaPath(position, mediaPath, cancelFlag);
        }
    }

    /**
     * This method will give failure message
     * @param message the reason for the failure
     */
    @Override
    public void onMediaDataFailure(String message) {
        if (view != null) {
            view.showMessage(message);
        }
    }

    /**
     * This method will be called if there is a network error.
     */
    @Override
    public void onNetworkError() {
        if (view != null) {
            view.showMessage("Please check your network connection");
        }
    }
}
