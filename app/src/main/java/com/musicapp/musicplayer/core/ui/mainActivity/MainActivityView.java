package com.musicapp.musicplayer.core.ui.mainActivity;

import com.musicapp.musicplayer.core.models.TrackListModel;

import java.util.List;

/**
 * This is the view interface for the MainActivity.
 *
 * @author Akhil Aravind
 */
public interface MainActivityView {

    /**
     * This method will save the tracks to preference.
     *@param position the position of that track
     *@param mediaPath media URL
     */
    void saveMediaPath(int position, String mediaPath);

    /**
     * This method will show progress dialogs.
     */
    void showProgressDialog();

    /**
     * This method will hide progress dialogs.
     */
    void hideProgressDialog();

    /**
     * This method will show toasts.
     * @param message the message to be toasted
     */
    void showMessage(String message);

    /**
     * This method get the Dhwani tracks
     * @param myTrackList the track list
     */
    void onFetchAudioDataReceived(List<TrackListModel> myTrackList);

    /**
     * This method get the Dhyana tracks
     * @param myTrackList the track list
     */
    void onFetchDhyanaDataReceived(List<TrackListModel> myTrackList);

    /**
     * This method get the Prana tracks
     * @param myTrackList the track list
     */
    void onFetchPranaDataReceived(List<TrackListModel> myTrackList);
}

