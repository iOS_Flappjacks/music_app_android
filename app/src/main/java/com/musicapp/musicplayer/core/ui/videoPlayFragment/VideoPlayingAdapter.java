package com.musicapp.musicplayer.core.ui.videoPlayFragment;

/**
 * This adapter is used to display the list of Dhyana and Prana tracks.
 *
 * @author Akhil Aravind
 */
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.musicapp.musicplayer.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class VideoPlayingAdapter extends RecyclerView.Adapter<VideoPlayingAdapter.SongListAdapterViewHolder> {

    /**
     * Initialising the local variables
     */
    private List<String> durationList;
    private List<String> mediaList;
    private List<String> imageList;
    private List<String> mediaTitleList;
    private List<String> ratingList;
    Context context;
    RecyclerViewClickInterfaceVideoPlay listener;
    int highlightPosition = 0;

    /**
     * This is an constructor used to initialise the arraylist and the context
     *
     * @param context      This param is used to specify the context.
     * @param mediaList      This is an array list holds the list of tracks url
     * @param offlineMediaImageList   This is an array list holds the list of background images.
     * @param mediaDurationList   This is an array list holds the list of durations of tracks.
     * @param mediaTitleList   This is an array list holds the list of titles.
     * @param ratingList   This is an array list holds the list of ratings.
     * @param listener recyclerView click listener
     */
    public VideoPlayingAdapter(Context context, List<String> mediaList, List<String> offlineMediaImageList,
                               List<String> mediaDurationList, List<String> mediaTitleList, List<String> ratingList, int highlightPosition, RecyclerViewClickInterfaceVideoPlay listener) {
        this.mediaList = mediaList;
        this.imageList = offlineMediaImageList;
        this.durationList = mediaDurationList;
        this.mediaTitleList = mediaTitleList;
        this.ratingList = ratingList;
        this.context = context;
        this.listener = listener;
        this.highlightPosition = highlightPosition;
    }

    @Override
    public SongListAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View guideListView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_song_list, parent, false);
        SongListAdapterViewHolder holder = new SongListAdapterViewHolder(guideListView);
        return holder;
    }

    @Override
    public void onBindViewHolder(final SongListAdapterViewHolder holder, final int position) {

        Glide.with(context).load(imageList.get(position)).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

                    holder.ivUpcomingMedia.setClipToOutline(true);
                    holder.ivUpcomingMedia.setImageDrawable(resource);

//                    BitmapDrawable background = new BitmapDrawable(ImageHelper.getRoundedCornerBitmap(drawableToBitmap(resource),100));
//                    holder.ivUpcomingMedia.setImageDrawable(background);
                }
            }
        });

//        Bitmap icon = BitmapFactory.decodeResource(context.getResources(), imageList.get(position));
//        BitmapDrawable background = new BitmapDrawable(ImageHelper.getRoundedCornerBitmap(icon,100));
//        holder.ivUpcomingMedia.setImageDrawable(background);

        Log.e("HighlightPosition", String.valueOf(highlightPosition) + "------");
        Log.e("CurrentPosition", String.valueOf(position) + "------");

        if (highlightPosition == position) {
            holder.rlParent.setBackground(context.getResources().getDrawable(R.drawable.selected_gradient));
        } else {
            holder.rlParent.setBackground(context.getResources().getDrawable(R.drawable.box1_bg));
        }

        holder.tvDuration.setText(durationList.get(position));
        holder.tvRating.setText(ratingList.get(position));

        if (!mediaList.get(position).contains("MusicPlayerDemo")) {
            holder.ivDownloadMedia.setVisibility(View.VISIBLE);

//            String path = mediaList.get(position);
//            String filename=path.substring(path.lastIndexOf("/")+1);
            holder.tvMediaName.setText(mediaTitleList.get(position));

        } else {
            holder.ivDownloadMedia.setVisibility(View.INVISIBLE);

//            String path = mediaList.get(position);
//            String filename=path.substring(path.lastIndexOf("/")+1);
            holder.tvMediaName.setText(mediaTitleList.get(position));
        }

        holder.rlParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.recyclerviewOnClick(position, mediaList.get(position), imageList.get(position));
            }
        });

        holder.ivDownloadMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.recyclerviewOnClickDownload(position, mediaList.get(position));
            }
        });

        holder.ivRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.recyclerviewOnClickRate(position);
            }
        });

    }

    /**
     * This methos refreshes the background of that selected song with a gradient background.
     */
    public void refresh(int highlightPosition)
    {
        this.highlightPosition = highlightPosition;
        notifyDataSetChanged();
    }

    /**
     * This method is used to show the count of the views.
     *
     * @return the size of the list.
     */
    @Override
    public int getItemCount() {
        return mediaList.size();
    }

    /**
     * ViewHolder class is used to initialise the custom adapter objects.
     */
    public class SongListAdapterViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout rlParent;
        ImageView ivUpcomingMedia, ivDownloadMedia, ivRating;
        TextView tvMediaName, tvDuration, tvRating;

        public SongListAdapterViewHolder(View view) {
            super(view);
            rlParent = view.findViewById(R.id.rl_parent_csl);
            ivUpcomingMedia = view.findViewById(R.id.iv_media_csl);
            ivDownloadMedia = view.findViewById(R.id.iv_download_csl);
            tvMediaName = view.findViewById(R.id.tv_media_name_csl);
            tvDuration = view.findViewById(R.id.tv_media_time_csl);
            ivRating = view.findViewById(R.id.iv_rate_csl);
            tvRating = view.findViewById(R.id.tv_rating_csl);
        }
    }

    /**
     * This method is used to get the duration of the file
     * @param file The file path
     * @param context Context of the class
     * @return Duration of the media file
     */
    public static String getFileDuration(Context context, File file) {
        String result = null;
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            FileInputStream inputStream = new FileInputStream(file.getAbsolutePath());
            retriever.setDataSource(inputStream.getFD());
            long time = Long.parseLong(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));

            int millSecond = Integer.parseInt(String.valueOf(time));

            String t1, t2;
            t1 = String.valueOf(TimeUnit.MILLISECONDS.toMinutes((long) millSecond));
            if (t1.length() == 1) {
                t1 = 0 + t1;
            }

            t2 = String.valueOf(TimeUnit.MILLISECONDS.toSeconds((long) millSecond) -
                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) millSecond)));
            if (t2.length() == 1) {
                t2 = 0 + t2;
            }
            result = t1 + " : " + t2;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * This method is used to convert the drawable to Bitmap.
     *
     * @param drawable drawable to be converted
     * @return the bitmap of corresponding drawable.
     */
    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
}
