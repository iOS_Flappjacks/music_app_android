package com.musicapp.musicplayer.core.ui.nowPlayingFragment;

import android.util.Log;

import com.musicapp.musicplayer.services.NetworkService;
import com.musicapp.musicplayer.services.StorageService;

/**
 * This is the interactor implementation for the nowPlayingFragment.
 *
 * @author Akhil Aravind
 */
public class NowPlayingInteractorImpl implements NowPlayingInteractor,NetworkService.NetworkServiceListener {

    private NetworkService networkService;
    private StorageService storageService;
    private MediaDataListener listener;

    /**
     * This method will download the track
     * @param mediaUrl the url to be downloaded
     * @param listener MediaDataListener instance
     */
    @Override
    public void downloadMedia(String mediaUrl, MediaDataListener listener) {
        this.listener = listener;

        if(networkService.haveNetworkAccess())
        {
            networkService.sendMediaDownloadRequest(mediaUrl,this);
        }else
        {
            listener.onNetworkError();
        }
    }

    /**
     * This method will initialise the network and storage services
     * @param networkService the Network service
     * @param storageService the Storage service
     */
    @Override
    public void inject(NetworkService networkService, StorageService storageService) {
        this.networkService = networkService;
        this.storageService = storageService;
    }

    /**
     * This method will be called when the request is failed.
     * @param response The failure response
     */
    @Override
    public void onFailure(String response) {

        listener.onMediaDataFailure(response);
    }

    /**
     * This method will be called when the request is success.
     * @param response The downloaded file path
     * @param cancelFlag The flag to recognise whether user has intentionally cancelled or not
     */
    @Override
    public void onSuccess(String response, Boolean cancelFlag) {
        Log.e(this.getClass().getSimpleName(),response);
        Log.e("onSuccess", "1111111111111");

        if (listener != null) {

            listener.onMediaDataReceived(response, cancelFlag);

        }
    }

}
