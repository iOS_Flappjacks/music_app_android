package com.musicapp.musicplayer.core.ui.mainActivity;

import android.util.Log;

import com.musicapp.musicplayer.core.models.TrackListModel;
import com.musicapp.musicplayer.services.NetworkService;
import com.musicapp.musicplayer.services.StorageService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the interactor implementation for the mainActivity.
 *
 * @author Akhil Aravind
 */
public class MainActivityInteractorImpl implements MainActivityInteractor,NetworkService.NetworkServiceListener {

    private NetworkService networkService;
    private StorageService storageService;
    private FetchAudioDataListener audioDataListener;
    private FetchDhyanaDataListener dhyanaDataListener;
    private FetchPranaDataListener pranaDataListener;
    private List<TrackListModel> myTrackList = new ArrayList<>();

    /**
     * This method will fetch the Dhwani tracks.
     * @param listener FetchAudioDataListener instance
     */
    @Override
    public void fetchAudioTracks(FetchAudioDataListener listener) {
        this.audioDataListener = listener;
        this.dhyanaDataListener = null;
        this.pranaDataListener = null;

        if(networkService.haveNetworkAccess())
        {
            networkService.sendFetchAudioRequest(this);
        }else
        {
            listener.onNetworkError();
        }
    }

    /**
     * This method will fetch the Dhyana tracks.
     * @param listener FetchDhyanaDataListener instance
     */
    @Override
    public void fetchDhyanaTracks(FetchDhyanaDataListener listener) {
        this.dhyanaDataListener = listener;
        this.audioDataListener = null;
        this.pranaDataListener = null;

        if(networkService.haveNetworkAccess())
        {
            networkService.sendFetchDhyanaRequest(this);
        }else
        {
            listener.onNetworkError();
        }
    }

    /**
     * This method will fetch the Prana tracks.
     * @param listener FetchPranaDataListener instance
     */
    @Override
    public void fetchPranaTracks(FetchPranaDataListener listener) {
        this.pranaDataListener = listener;
        this.dhyanaDataListener = null;
        this.audioDataListener = null;

        if(networkService.haveNetworkAccess())
        {
            networkService.sendFetchPranaRequest(this);
        }else
        {
            listener.onNetworkError();
        }
    }

    /**
     * This method will initialise the network and storage services
     * @param networkService the Network service
     * @param storageService the Storage service
     */
    @Override
    public void inject(NetworkService networkService, StorageService storageService) {
        this.networkService = networkService;
        this.storageService = storageService;
    }

    /**
     * This method will be called when the request is failed.
     * @param response The failure response
     */
    @Override
    public void onFailure(String response) {

        if (audioDataListener != null) {
            audioDataListener.onFetchAudioFailure(response);
        } else if(dhyanaDataListener != null) {
            dhyanaDataListener.onFetchDhyanaFailure(response);
        } else if(pranaDataListener != null) {
            pranaDataListener.onFetchPranaFailure(response);
        }
    }

    /**
     * This method will be called when the request is success.
     * @param response The success response
     * @param cancelFlag The flag to recognise whether user has intentionally cancelled or not
     */
    @Override
    public void onSuccess(String response, Boolean cancelFlag) {
//        Log.e(this.getClass().getSimpleName(),response);
        Log.e("onSuccess", "1111111111111");

        if (audioDataListener != null) {
            try {

                JSONObject object = new JSONObject(response);
                myTrackList.clear();
                JSONArray dataArray = object.getJSONArray("track_list");

                for (int i = 0; i<dataArray.length();i++) {
                    JSONObject dataObject = dataArray.getJSONObject(i);

                    int track_id = dataObject.getInt("track_id");
                    String track_type = dataObject.getString("track_type");
                    String uuid = "";
                    if (response.contains("uuid")) {
                        uuid = dataObject.getString("uuid");
                    } else {
                        uuid = dataObject.getString("track_id");
                    }                    String url = dataObject.getString("url");
                    String author = dataObject.getString("author");
                    String album = dataObject.getString("album");
                    String title = dataObject.getString("title");
                    String description = dataObject.getString("description");
                    String length = dataObject.getString("length");
                    String thumbnail = dataObject.getString("thumbnail");
                    String background_pic = dataObject.getString("background_pic");

                    TrackListModel model = new TrackListModel();
                    model.setTrack_id(track_id);
                    model.setUuid(uuid);
                    model.setTrack_type(track_type);
                    model.setUrl(url);
                    model.setAuthor(author);
                    model.setUserRating("0.0");
                    model.setAlbum(album);
                    model.setTitle(title);
                    model.setDescription(description);
                    model.setLength(length);
                    model.setThumbnail(thumbnail);
                    model.setBackground_pic(background_pic);

                    myTrackList.add(model);
                }
                audioDataListener.onFetchAudioDataReceived(myTrackList);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (dhyanaDataListener != null) {
            try {

                JSONObject object = new JSONObject(response);
                myTrackList.clear();
                JSONArray dataArray = object.getJSONArray("track_list");

                for (int i = 0; i<dataArray.length();i++) {
                    JSONObject dataObject = dataArray.getJSONObject(i);

                    int track_id = dataObject.getInt("track_id");
                    String track_type = dataObject.getString("track_type");
                    String uuid = "";
                    if (response.contains("uuid")) {
                        uuid = dataObject.getString("uuid");
                    } else {
                        uuid = dataObject.getString("track_id");
                    }                    String url = dataObject.getString("url");
                    String author = dataObject.getString("author");
                    String album = dataObject.getString("album");
                    String title = dataObject.getString("title");
                    String description = dataObject.getString("description");
                    String length = dataObject.getString("length");
                    String thumbnail = dataObject.getString("thumbnail");
                    String background_pic = dataObject.getString("background_pic");

                    TrackListModel model = new TrackListModel();
                    model.setTrack_id(track_id);
                    model.setUuid(uuid);
                    model.setTrack_type(track_type);
                    model.setUrl(url);
                    model.setAuthor(author);
                    model.setUserRating("0.0");
                    model.setAlbum(album);
                    model.setTitle(title);
                    model.setDescription(description);
                    model.setLength(length);
                    model.setThumbnail(thumbnail);
                    model.setBackground_pic(background_pic);

                    myTrackList.add(model);
                }
                dhyanaDataListener.onFetchDhyanaDataReceived(myTrackList);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (pranaDataListener != null) {
            try {

                JSONObject object = new JSONObject(response);
                myTrackList.clear();
                JSONArray dataArray = object.getJSONArray("track_list");

                for (int i = 0; i<dataArray.length();i++) {
                    JSONObject dataObject = dataArray.getJSONObject(i);

                    int track_id = dataObject.getInt("track_id");
                    String uuid = "";
                    if (response.contains("uuid")) {
                        uuid = dataObject.getString("uuid");
                    } else {
                        uuid = dataObject.getString("track_id");
                    }
                    String track_type = dataObject.getString("track_type");
                    String url = dataObject.getString("url");
                    String author = dataObject.getString("author");
                    String album = dataObject.getString("album");
                    String title = dataObject.getString("title");
                    String description = dataObject.getString("description");
                    String length = dataObject.getString("length");
                    String thumbnail = dataObject.getString("thumbnail");
                    String background_pic = dataObject.getString("background_pic");

                    TrackListModel model = new TrackListModel();
                    model.setTrack_id(track_id);
                    model.setUuid(uuid);
                    model.setTrack_type(track_type);
                    model.setUrl(url);
                    model.setAuthor(author);
                    model.setUserRating("0.0");
                    model.setAlbum(album);
                    model.setTitle(title);
                    model.setDescription(description);
                    model.setLength(length);
                    model.setThumbnail(thumbnail);
                    model.setBackground_pic(background_pic);

                    myTrackList.add(model);
                }
                pranaDataListener.onFetchPranaDataReceived(myTrackList);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
