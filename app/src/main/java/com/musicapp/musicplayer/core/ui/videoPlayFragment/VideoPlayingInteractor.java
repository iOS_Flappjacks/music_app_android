package com.musicapp.musicplayer.core.ui.videoPlayFragment;

import com.musicapp.musicplayer.services.NetworkService;
import com.musicapp.musicplayer.services.StorageService;

/**
 * This is the interactor interface for the videoPlayFragment.
 *
 * @author Akhil Aravind
 */
public interface VideoPlayingInteractor
{
    /**
     * This method will initialise the network and storage services
     * @param networkService the Network service
     * @param storageService the Storage service
     */
    void inject(NetworkService networkService, StorageService storageService);

    /**
     * This method will download the track
     * @param mediaUrl the url to be downloaded
     * @param listener MediaDataListener instance
     */
    void downloadMedia(String mediaUrl, VideoPlayingInteractor.MediaDataListener listener);

    /**
     * Interface to handle the downloaded track data
     */
    interface MediaDataListener
    {
        /**
         * This method will give the downloaded track
         * @param mediaPath the path where the file was saved
         * @param cancelFlag Flag to check whether user have cancelled the download or not
         */
        void onMediaDataReceived(String mediaPath, Boolean cancelFlag);

        /**
         * This method will give failure message
         * @param message the reason for the failure
         */
        void onMediaDataFailure(String message);

        /**
         * This method will be called if there is a network error.
         */
        void onNetworkError();
    }

}
