package com.musicapp.musicplayer.core.utilities;

/**
 * This class is used to Store the URL's and the constants used in this app.
 * @author Akhil Aravind
 *
 */

public class AppConstants {

    public static String MEDIA_URL_1 = "https://s3.ap-south-1.amazonaws.com/sample-playlist/Kaveri+river+flowing.wav";
    public static String MEDIA_URL_2 = "https://s3.ap-south-1.amazonaws.com/sample-playlist/Kaveri+stream+flowing.wav";
    public static String MEDIA_URL_3 = "https://s3.ap-south-1.amazonaws.com/sample-playlist/Ranganthittu+Birds+1.wav";
    public static String MEDIA_URL_4 = "https://s3.ap-south-1.amazonaws.com/sample-playlist/Ranganthittu+Birds+2.wav";

    public static String MEDIA_URL_5 = "https://s3.ap-south-1.amazonaws.com/sample-playlist/Videos/2.mp4";
    public static String MEDIA_URL_6 = "https://www.demonuts.com/Demonuts/smallvideo.mp4";
    public static String MEDIA_URL_7 = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";
    public static String MEDIA_URL_8 = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4";

    public static String BASE_URL = "https://thefullerlife.com/mfl/";

    public static String MINDFUL_MOVEMENT = "mindful_movement_track_list.php";

    public static String DHYANA = "dhyana_track_list.php";

    public static String PRANA = "prana_track_list.php";

}
