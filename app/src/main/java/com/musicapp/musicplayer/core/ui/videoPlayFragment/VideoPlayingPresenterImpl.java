package com.musicapp.musicplayer.core.ui.videoPlayFragment;

import com.musicapp.musicplayer.services.NetworkService;
import com.musicapp.musicplayer.services.StorageService;

/**
 * This is the presenter implementation for the videoPlayFragment.
 *
 * @author Akhil Aravind
 */
public class VideoPlayingPresenterImpl implements VideoPlayingPresenter, VideoPlayingInteractor.MediaDataListener{
    private VideoPlayingFragmentView view;
    private VideoPlayingInteractor interactor;
    private int position = 0;

    VideoPlayingPresenterImpl(VideoPlayingFragmentView view) {
        this.view = view;
        interactor = new VideoPlayingInteractorImpl();
    }

    /**
     * This method will download the track
     * @param position the position of the current downloaded track
     * @param mediaUrl the url to be downloaded
     */
    @Override
    public void doDownloadMedia(int position, String mediaUrl) {
        this.position = position;
        if (view != null) {
            if (interactor != null) {
//                view.showProgressDialog();
                interactor.downloadMedia(mediaUrl,this);
            }
        }
    }

    /**
     * This method will load the url
     * @param currentPlaying the url to be loaded
     */
    @Override
    public void doLoadMedia(String currentPlaying) {
        if (view != null) {
            view.loadMedia(currentPlaying);
        }
    }

    /**
     * This method will initialise the network and storage services
     * @param networkService the Network service
     * @param storageService the Storage service
     */
    @Override
    public void inject(NetworkService networkService, StorageService storageService) {
        interactor.inject(networkService, storageService);
    }

    /**
     * This method will give the downloaded track
     * @param mediaPath the path where the file was saved
     * @param cancelFlag Flag to check whether user have cancelled the download or not
     */
    @Override
    public void onMediaDataReceived(String mediaPath, Boolean cancelFlag) {
        if (view != null) {
            view.saveMediaPath(position, mediaPath, cancelFlag);
        }
    }

    /**
     * This method will give failure message
     * @param message the reason for the failure
     */
    @Override
    public void onMediaDataFailure(String message) {
        if (view != null) {
            view.showMessage(message);
        }
    }

    /**
     * This method will be called if there is a network error.
     */
    @Override
    public void onNetworkError() {
        if (view != null) {
            view.showMessage("Please check your network connection");
        }
    }
}
