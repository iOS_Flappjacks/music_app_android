package com.musicapp.musicplayer.core.ui.mainActivity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.musicapp.musicplayer.R;

/**
 * This fragment is the base fragment for all fragments.
 *
 * @author Akhil Aravind
 */
public class BaseFragment extends Fragment {
    private FragmentManager fm;

    public  BaseFragment(){

    }
    protected static FragmentActivity mActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    /**
     * This method is basic onResume method which will handle the maximize or starting events of views
     */
    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * This method will call when the view is destroyed
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * This method will call when the view is attached to an activity
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    /**
     * This method will call when the view is attached to an activity
     */
    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        mActivity=(FragmentActivity)context;

    }

    /**
     * This method is used to set the fragments on the fragment container
     */
    public void setFragment(Fragment frag) {

        if (frag != null) {
            fm = mActivity.getSupportFragmentManager();

            try {
                fm.beginTransaction()

                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right )
                        .replace(R.id.fl_container_main, frag)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack(frag.getClass().getName())
                        .commit();
            } catch(Exception e) {
                e.printStackTrace();
            }
        } else {
            // error in creating fragment
            Log.e("fragment", "Error in creating fragment");
        }
    }

    /**
     * This method is used to set the fragments on the fragment container without adding to backstack
     */
    public void setFragmentWithoutBackstack(Fragment frag) {

        if (frag != null) {
            fm = mActivity.getSupportFragmentManager();

            try {
                fm.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right )
                        .replace(R.id.fl_container_main, frag)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            // error in creating fragment
            Log.e("fragment", "Error in creating fragment");
        }
    }
}
