package com.musicapp.musicplayer.core.ui.mainActivity;

import com.musicapp.musicplayer.core.models.TrackListModel;
import com.musicapp.musicplayer.services.NetworkService;
import com.musicapp.musicplayer.services.StorageService;

import java.util.List;

/**
 * This is the interactor interface for the mainActivity.
 *
 * @author Akhil Aravind
 */
public interface MainActivityInteractor
{
    /**
     * This method will initialise the network and storage services
     * @param networkService the Network service
     * @param storageService the Storage service
     */
    void inject(NetworkService networkService, StorageService storageService);

    /**
     * This method will fetch the Dhwani tracks.
     * @param listener FetchAudioDataListener instance
     */
    void fetchAudioTracks(MainActivityInteractor.FetchAudioDataListener listener);

    /**
     * This method will fetch the Dhyana tracks.
     * @param listener FetchDhyanaDataListener instance
     */
    void fetchDhyanaTracks(MainActivityInteractor.FetchDhyanaDataListener listener);

    /**
     * This method will fetch the Prana tracks.
     * @param listener FetchPranaDataListener instance
     */
    void fetchPranaTracks(MainActivityInteractor.FetchPranaDataListener listener);

    /**
     * Interface to handle the fetch Dhwani track data
     */
    interface FetchAudioDataListener
    {
        /**
         * This method will give the tracks
         * @param myTrackList the arraylist of TrackListModel
         */
        void onFetchAudioDataReceived(List<TrackListModel> myTrackList);

        /**
         * This method will give failure message
         * @param message the reason for the failure
         */
        void onFetchAudioFailure(String message);

        /**
         * This method will be called if there is a network error.
         */
        void onNetworkError();
    }

    /**
     * Interface to handle the fetch Dhyana track data
     */
    interface FetchDhyanaDataListener
    {
        /**
         * This method will give the tracks
         * @param myTrackList the arraylist of TrackListModel
         */
        void onFetchDhyanaDataReceived(List<TrackListModel> myTrackList);

        /**
         * This method will give failure message
         * @param message the reason for the failure
         */
        void onFetchDhyanaFailure(String message);

        /**
         * This method will be called if there is a network error.
         */
        void onNetworkError();
    }

    /**
     * Interface to handle the fetch Prana track data
     */
    interface FetchPranaDataListener
    {
        /**
         * This method will give the tracks
         * @param myTrackList the arraylist of TrackListModel
         */
        void onFetchPranaDataReceived(List<TrackListModel> myTrackList);

        /**
         * This method will give failure message
         * @param message the reason for the failure
         */
        void onFetchPranaFailure(String message);

        /**
         * This method will be called if there is a network error.
         */
        void onNetworkError();
    }

}
