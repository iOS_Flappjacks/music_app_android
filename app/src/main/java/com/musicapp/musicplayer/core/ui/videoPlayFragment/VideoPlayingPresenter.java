package com.musicapp.musicplayer.core.ui.videoPlayFragment;

import com.musicapp.musicplayer.services.NetworkService;
import com.musicapp.musicplayer.services.StorageService;

/**
 * This is the presenter interface for the videoPlayFragment.
 *
 * @author Akhil Aravind
 */
public interface VideoPlayingPresenter {

    /**
     * This method will initialise the network and storage services
     * @param networkService the Network service
     * @param storageService the Storage service
     */
    void inject(NetworkService networkService, StorageService storageService);

    /**
     * This method will download the track
     * @param position the position of the current downloaded track
     * @param mediaUrl the url to be downloaded
     */
    void doDownloadMedia(int position, String mediaUrl);

    /**
     * This method will load the url
     * @param currentPlaying the url to be loaded
     */
    void doLoadMedia(String currentPlaying);

}
