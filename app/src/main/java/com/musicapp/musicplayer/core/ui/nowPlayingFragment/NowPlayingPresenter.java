package com.musicapp.musicplayer.core.ui.nowPlayingFragment;

import android.media.MediaPlayer;

import com.musicapp.musicplayer.services.NetworkService;
import com.musicapp.musicplayer.services.StorageService;

/**
 * This is the presenter interface for the nowPlayingFragment.
 *
 * @author Akhil Aravind
 */
public interface NowPlayingPresenter {

    /**
     * This method will initialise the network and storage services
     * @param networkService the Network service
     * @param storageService the Storage service
     */
    void inject(NetworkService networkService, StorageService storageService);

    /**
     * This method will download the track
     * @param position the position of the current downloaded track
     * @param mediaUrl the url to be downloaded
     */
    void doDownloadMedia(int position, String mediaUrl);

    /**
     * This method will fetch the song details
     * @param mediaPlayer The mediaplayer variable to fetch the song details
     */
    void fetchSongDetails(MediaPlayer mediaPlayer);

    /**
     * This method will load the url
     * @param currentPlaying the url to be loaded
     */
    void doLoadMedia(String currentPlaying);

    /**
     * This method will show progress dialogs.
     */
    void showProgressDialog();

    /**
     * This method will hide progress dialogs.
     */
    void hideProgressDialog();

    /**
     * This method will switch to next track
     */
    void doSwitchNext();

    /**
     * This method will switch to previous track
     */
    void doSwitchPrevious();

    /**
     * This method will pause the track
     */
    void doPauseMedia();

    /**
     * This method will play the track
     */
    void doPlayMedia();
}
