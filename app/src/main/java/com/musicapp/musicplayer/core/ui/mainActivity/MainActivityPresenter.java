package com.musicapp.musicplayer.core.ui.mainActivity;

import com.musicapp.musicplayer.services.NetworkService;
import com.musicapp.musicplayer.services.StorageService;

/**
 * This is the presenter interface for the mainActivity.
 *
 * @author Akhil Aravind
 */
public interface MainActivityPresenter {

    /**
     * This method will initialise the network and storage services
     * @param networkService the Network service
     * @param storageService the Storage service
     */
    void inject(NetworkService networkService, StorageService storageService);

    /**
     * This method will show progress dialogs.
     */
    void showProgressDialog();

    /**
     * This method will hide progress dialogs.
     */
    void hideProgressDialog();

    /**
     * This method will fetch the Dhwani tracks from server
     */
    void fetchAudioTracks();

    /**
     * This method will fetch the Dhyana tracks from server
     */
    void fetchDhyanaTracks();

    /**
     * This method will fetch the Prana. tracks from server
     */
    void fetchPranaTracks();
}
