package com.musicapp.musicplayer.core.models;

/**
 * TrackListModel is a pojo class used to hold the values for every track details
 * @author Akhil Aravind
 */
public class TrackListModel {

    private String track_type = "", url = "", author = "", album = "", title = "", description = "", length = "", thumbnail = "", background_pic = "";
    int track_id = 0;
    private String uuid, userRating;

    /**
     * Method used to get the user rating
     */
    public String getUserRating() {
        return userRating;
    }

    /**
     * Method used to set the rating for that track.
     * @param  userRating user rating .
     */
    public void setUserRating(String userRating) {
        this.userRating = userRating;
    }

    /**
     * Method used to get the track type
     */
    public String getTrack_type() {
        return track_type;
    }

    /**
     * Method used to set the track type.
     * @param  track_type type of the track.
     */
    public void setTrack_type(String track_type) {
        this.track_type = track_type;
    }

    /**
     * Method used to get the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Method used to set url of track.
     * @param  url track url.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Method used to get the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Method used to set the author name.
     * @param  author author name of the track.
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Method used to get the Album
     */
    public String getAlbum() {
        return album;
    }

    /**
     * Method used to set the album name of track.
     * @param  album album name.
     */
    public void setAlbum(String album) {
        this.album = album;
    }

    /**
     * Method used to get the Title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Method used to set the title of the track.
     * @param  title track title.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Method used to get the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Method used to set the description of the track.
     * @param  description track descripion.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Method used to get the length
     */
    public String getLength() {
        return length;
    }

    /**
     * Method used to set the duration of the track.
     * @param  length duration of the track.
     */
    public void setLength(String length) {
        this.length = length;
    }

    /**
     * Method used to get the thumbnail image
     */
    public String getThumbnail() {
        return thumbnail;
    }

    /**
     * Method used to set the thumbnail picture.
     * @param  thumbnail thumbnail picture url.
     */
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    /**
     * Method used to get the background image
     */
    public String getBackground_pic() {
        return background_pic;
    }

    /**
     * Method used to set the background picture.
     * @param  background_pic background picture url.
     */
    public void setBackground_pic(String background_pic) {
        this.background_pic = background_pic;
    }

    /**
     * Method used to get the track id
     */
    public int getTrack_id() {
        return track_id;
    }

    /**
     * Method used to set the track id
     * @param  track_id track id for that track.
     */
    public void setTrack_id(int track_id) {
        this.track_id = track_id;
    }

    /**
     * Method used to set the unique UUID
     * @param  uuid UUID of that track.
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Method used to get the unique UUID
     */
    public String getUuid() {
        return uuid;
    }
}
