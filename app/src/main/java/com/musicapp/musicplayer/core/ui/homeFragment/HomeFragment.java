package com.musicapp.musicplayer.core.ui.homeFragment;

/**
 * This fragment is used to display the home fragment screen design.
 *
 * @author Akhil Aravind
 */
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.musicapp.musicplayer.R;
import com.musicapp.musicplayer.core.ui.mainActivity.BaseFragment;
import com.musicapp.musicplayer.core.ui.nowPlayingFragment.NowPlayingFragment;
import com.musicapp.musicplayer.core.ui.videoPlayFragment.VideoPlayingFragment;

import static com.musicapp.musicplayer.core.ui.mainActivity.MainActivity.sansRegular;
import static com.musicapp.musicplayer.core.ui.mainActivity.MainActivity.sansSemiBold;
import static com.musicapp.musicplayer.core.ui.mainActivity.MainActivity.sanslight;

public class HomeFragment extends BaseFragment implements View.OnClickListener{

    /**
     * Declaring the variables
     */
    private View rootView;
    LinearLayout llMindfulEating, llMindfulMovement, llDhyana, llPrana;
    TextView textView1, textView2, tvEating, tvMovement, tvDhyana, tvPrana;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home_screen, container, false);

        llMindfulEating = rootView.findViewById(R.id.ll_mindful_eating);
        llMindfulMovement = rootView.findViewById(R.id.ll_mindful_movement);
        llDhyana = rootView.findViewById(R.id.ll_dhyana);
        llPrana = rootView.findViewById(R.id.ll_prana);
        textView1 = rootView.findViewById(R.id.textview1_fhs);
        textView2 = rootView.findViewById(R.id.textview2_fhs);
        tvEating = rootView.findViewById(R.id.textview_eating_fhs);
        tvMovement = rootView.findViewById(R.id.textview_movement_fhs);
        tvDhyana = rootView.findViewById(R.id.textview_dhyana_fhs);
        tvPrana = rootView.findViewById(R.id.textview_prana_fhs);

        /**
         * Setting the screen fonts
         */
        textView1.setTypeface(sanslight);
        textView2.setTypeface(sansRegular);
        tvEating.setTypeface(sansSemiBold);
        tvMovement.setTypeface(sansSemiBold);
        tvDhyana.setTypeface(sansSemiBold);
        tvPrana.setTypeface(sansSemiBold);

        llMindfulEating.setOnClickListener(this);
        llMindfulMovement.setOnClickListener(this);
        llDhyana.setOnClickListener(this);
        llPrana.setOnClickListener(this);

        return rootView;
    }

    /**
     * This method handles all the onClick events
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_mindful_eating:
//                setFragmentWithoutBackstack(new VideoPlayingFragment());
                break;
            case R.id.ll_mindful_movement:
                Fragment movement = new NowPlayingFragment();
//                Bundle bundleMovement = new Bundle();
//                bundleMovement.putString("coming_from", "music");
//                movement.setArguments(bundleMovement);
                setFragment(movement);
                break;
            case R.id.ll_dhyana:
                Fragment dhyana = new VideoPlayingFragment();
                Bundle bundleDhyana = new Bundle();
//                bundleDhyana.putString("coming_from", "video");
                bundleDhyana.putString("header", "Dhyana");
                dhyana.setArguments(bundleDhyana);
                setFragment(dhyana);
                break;
            case R.id.ll_prana:
                Fragment prana = new VideoPlayingFragment();
                Bundle bundlePrana = new Bundle();
//                bundlePrana.putString("coming_from", "video");
                bundlePrana.putString("header", "Prana");
                prana.setArguments(bundlePrana);
                setFragment(prana);
                break;
        }
    }
}
