package com.musicapp.musicplayer.core.ui.videoPlayFragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.musicapp.musicplayer.R;
import com.musicapp.musicplayer.core.models.TrackListModel;
import com.musicapp.musicplayer.core.ui.homeFragment.HomeFragment;
import com.musicapp.musicplayer.core.ui.mainActivity.BaseFragment;
import com.musicapp.musicplayer.core.utilities.DialogManager;
import com.musicapp.musicplayer.services.NetworkService;
import com.musicapp.musicplayer.services.StorageService;
import com.willy.ratingbar.BaseRatingBar;
import com.willy.ratingbar.ScaleRatingBar;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cn.jzvd.Jzvd;

import static com.musicapp.musicplayer.core.ui.mainActivity.MainActivity.haveNetworkAccess;
import static com.musicapp.musicplayer.core.ui.mainActivity.MainActivity.sansExtraBold;

/**
 * This fragment is used to display the Video Player screen
 *
 * @author Akhil Aravind
 */
public class VideoPlayingFragment extends BaseFragment implements RecyclerViewClickInterfaceVideoPlay,
        VideoPlayingFragmentView, View.OnClickListener {

    /**
     * Declaring the variables
     */
    private RecyclerView rvMedia;
    StorageService storageService;

    List<String> mediaPathList = new ArrayList<>();
    List<String> offlineMediaImageList = new ArrayList<String>();
    List<String> mediaDurationList = new ArrayList<String>();
    List<String> ratingList = new ArrayList<String>();
    List<String> mediaTitleList = new ArrayList<String>();

    private String mediaUrlPath = "";
    private VideoPlayingAdapter videoPlayingAdapter;
    private int position = 0;
    private RelativeLayout rlParent;
    private View rootView;
    private Dialog dialogProgress;
    private VideoPlayingPresenter presenter;
    private boolean autoPlay = false;
    private ProgressDialog pd;
    private FrameLayout flMain;
    private LottieAnimationView lottieAnimationView;
    Jzvd videoPlayer;
    ImageView ivHome, ivThumbnail;
    private boolean goToSettings = false;
    private boolean isFirstTime = true;
    private String comingFrom = "";
    TextView tvNoTracks;
    private List<TrackListModel> videoTrackList = new ArrayList<>();
    private Dialog dialog;
    final Float[] rating = {0.0F};
    private boolean isRatingDialogShown = false;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_video_playing, container, false);

        init();
        setListeners();
        return rootView;
    }

    /**
     * This method will initialize every variables
     * Initialize the VideoPlayingPresenter
     * Initialize Network service, progress dialog
     */
    private void init() {
        storageService = new StorageService(mActivity);

        presenter = new VideoPlayingPresenterImpl(this);
        NetworkService networkService = new NetworkService();
        networkService.inject(mActivity);
        presenter.inject(networkService, new StorageService(mActivity));

        dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_rate);

        flMain = rootView.findViewById(R.id.fl_parent_fvp);
        rlParent = rootView.findViewById(R.id.rl_parent_fvp);
        rvMedia = rootView.findViewById(R.id.rv_video_list_fvp);
        ivHome = rootView.findViewById(R.id.iv_home_fvp);
        tvNoTracks = rootView.findViewById(R.id.tv_no_tracks_fvp);

        videoPlayer = rootView.findViewById(R.id.video_player_fvp);
        ivThumbnail = rootView.findViewById(R.id.no_media_fvp);
        videoPlayer.clearFloatScreen();

        dialogProgress = DialogManager.getProgressDialog(mActivity);
        lottieAnimationView = dialogProgress.findViewById(R.id.lottie_cdp);

        flMain.setForeground(new ColorDrawable(ContextCompat.getColor(mActivity, R.color.transparentBlack)));

        LinearLayoutManager layoutManager= new LinearLayoutManager(mActivity,LinearLayoutManager.VERTICAL, false);
        rvMedia.setLayoutManager(layoutManager);

        Bundle bundle = getArguments();

        if (bundle != null) {
            comingFrom = bundle.getString("header");
        }

        /**
         * Checking whether Dhyana or Prana Screen.
         */
        if (comingFrom.equals("Dhyana")) {
            if (!storageService.getDhyanaTracks().equalsIgnoreCase("")) {
                Gson gson = new Gson();
                String jsonTracks = storageService.getDhyanaTracks();
                Type type = new TypeToken<ArrayList<TrackListModel>>() {}.getType();
                videoTrackList = gson.fromJson(jsonTracks, type);

                for (int i = 0; i < videoTrackList.size(); i++) {
                    mediaPathList.add(videoTrackList.get(i).getUrl());
                    offlineMediaImageList.add(videoTrackList.get(i).getThumbnail());
                    mediaDurationList.add(videoTrackList.get(i).getLength());
                    ratingList.add(videoTrackList.get(i).getUserRating());
                    if (videoTrackList.get(i).getTitle().length() != 0) {
                        mediaTitleList.add(videoTrackList.get(i).getTitle());
                    } else {
                        mediaTitleList.add(videoTrackList.get(i).getDescription());
                    }
                }
            }
        } else {
            if (!storageService.getPranaTracks().equalsIgnoreCase("")) {
                Gson gson = new Gson();
                String jsonTracks = storageService.getPranaTracks();
                Type type = new TypeToken<ArrayList<TrackListModel>>() {}.getType();
                videoTrackList = gson.fromJson(jsonTracks, type);

                for (int i = 0; i < videoTrackList.size(); i++) {
                    mediaPathList.add(videoTrackList.get(i).getUrl());
                    offlineMediaImageList.add(videoTrackList.get(i).getThumbnail());
                    mediaDurationList.add(videoTrackList.get(i).getLength());
                    ratingList.add(videoTrackList.get(i).getUserRating());
                    if (videoTrackList.get(i).getTitle().length() != 0) {
                        mediaTitleList.add(videoTrackList.get(i).getTitle());
                    } else {
                        mediaTitleList.add(videoTrackList.get(i).getDescription());
                    }
                }
            }
        }

//        if (mediaPathList.size() < 4) {
//            mediaPathList.add(AppConstants.MEDIA_URL_5);
//            mediaPathList.add(AppConstants.MEDIA_URL_6);
//            mediaPathList.add(AppConstants.MEDIA_URL_7);
//            mediaPathList.add(AppConstants.MEDIA_URL_8);
//
//            Gson gson = new Gson();
//            String jsonTracks = gson.toJson(mediaPathList);
//            storageService.storeDhyanaTracks(jsonTracks);
//        }

        Log.e("mediaPathList", mediaPathList.size() + "---");
        videoPlayingAdapter = new VideoPlayingAdapter(mActivity, mediaPathList, offlineMediaImageList, mediaDurationList, mediaTitleList, ratingList, position, this);
        rvMedia.setAdapter(videoPlayingAdapter);

//        Bundle bundle = getArguments();
//
//        if (bundle != null) {
//            position = bundle.getInt("now_playing");
//            mediaUrlPath = mediaPathList.get(bundle.getInt("now_playing"));
//            presenter.doLoadMedia(mediaUrlPath);
//        } else {
//            mediaUrlPath = mediaPathList.get(0);
//            presenter.doLoadMedia(mediaUrlPath);
//        }

        /**
         * Checking the tracklist whether it's empty or not
         */
        if (mediaPathList.size() != 0) {
            mediaUrlPath = mediaPathList.get(0);
            presenter.doLoadMedia(mediaUrlPath);
            videoPlayingAdapter.refresh(position);
        } else {
            rvMedia.setVisibility(View.GONE);
            tvNoTracks.setVisibility(View.VISIBLE);
            ivThumbnail.setVisibility(View.VISIBLE);
//            Toast.makeText(mActivity, "No tracks available", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Setting the listeners
     */
    @SuppressLint("ClickableViewAccessibility")
    private void setListeners() {
        ivHome.setOnClickListener(this);
        ivThumbnail.setOnClickListener(this);
    }

    /**
     * This method will handle all the onclick events on recycler list
     */
    @Override
    public void recyclerviewOnClick(int position, String mediaUrl, String mediaImage) {

        if (!mediaUrl.contains("MusicPlayerDemo")) {

            if (haveNetworkAccess(mActivity)) {
                autoPlay = true;
                this.position = position;
                mediaUrlPath = mediaUrl;
                videoPlayingAdapter.refresh(position);

                Log.e("recyclerviewOnClick", mediaPathList.get(position));
                presenter.doLoadMedia(mediaUrlPath);
            } else {
                showInternetDialog(mActivity);
            }
        } else {
            autoPlay = true;
            this.position = position;
            mediaUrlPath = mediaUrl;
            videoPlayingAdapter.refresh(position);

            Log.e("recyclerviewOnClick", mediaPathList.get(position));
            presenter.doLoadMedia(mediaUrlPath);
        }
    }

    /**
     * This method handles the click of download option
     */
    @Override
    public void recyclerviewOnClickDownload(int position, String mediaUrl) {

        presenter.doDownloadMedia(position, mediaUrl);
    }

    /**
     * This method handles the click of rate option
     */
    @Override
    public void recyclerviewOnClickRate(int position) {

        showRatingDialog(position);
    }

    /**
     * This method saves the tracks to the preference after download.
     */
    @Override
    public void saveMediaPath(int positionDownload, String mediaPath, Boolean cancelFlag) {

        Log.e("cancelFlag", cancelFlag + "111");
        if (!cancelFlag) {
            if (position == positionDownload) {
                mediaUrlPath = mediaPath;
            }

            mediaPathList.set(positionDownload, mediaPath);

            videoTrackList.get(positionDownload).setUrl(mediaPath);

            Gson gson = new Gson();
            String jsonTracks = gson.toJson(videoTrackList);

            if (comingFrom.equals("Dhyana")) {
                storageService.storeDhyanaTracks(jsonTracks);
            } else {
                storageService.storePranaTracks(jsonTracks);
            }

            videoPlayingAdapter.notifyDataSetChanged();
            Log.e("saveMediaPath", mediaPath);
        }
    }

    /**
     * This method toasts any messages.
     */
    @Override
    public void showMessage(String message) {
        if (message.contains("connection")) {
            showInternetDialog(mActivity);
        } else {
            Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This method is used to load the tracks and getting the music player ready to play.
     * This will handle the loading events and the delay in loading
     */
    @Override
    public void loadMedia(final String currentPlaying) {

        videoPlayer.resetProgressAndTime();
        videoPlayer.clearFloatScreen();

        if (videoPlayer.isCurrentPlay()) {
            videoPlayer.release();
        }

        if (!currentPlaying.contains("MusicPlayerDemo")) {

            if (haveNetworkAccess(mActivity)) {
                ivThumbnail.setVisibility(View.GONE);
                String filename = currentPlaying.substring(currentPlaying.lastIndexOf("/") + 1);

                videoPlayer.setUp(currentPlaying, mediaTitleList.get(position), Jzvd.SCREEN_WINDOW_NORMAL);
                if (autoPlay) {
                    videoPlayer.startVideo();
                }

                isRatingDialogShown = false;
                final Handler handler = new Handler();
                Runnable runnable = new Runnable() {

                    @Override
                    public void run() {
                        try{
                            if (videoPlayer.currentState == 6) {
                                if (!isRatingDialogShown) {
                                    if (ratingList.get(position).equals("0.0")) {
                                        showRatingDialog(position);
                                    } else {

                                    }
                                    isRatingDialogShown = true;
                                }
                            }
                        }
                        catch (Exception e) {

                        }
                        finally{
                            if (!isRatingDialogShown) {
                                handler.postDelayed(this, 500);
                            }
                        }
                    }
                };

                if (ratingList.get(position).equals("0.0")) {
                    handler.post(runnable);
                }  else {

                }

            } else {
                if (position == 0) {
                    ivThumbnail.setVisibility(View.VISIBLE);
                }
                    showInternetDialog(mActivity);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        mActivity.onBackPressed();
//                    }
//                }, 1000);
            }
        } else {
            ivThumbnail.setVisibility(View.GONE);

//            String filename= currentPlaying.substring(currentPlaying.lastIndexOf("/")+1);

            Uri fileUri = Uri.fromFile(new File(currentPlaying));

            videoPlayer.setUp(fileUri.toString() ,mediaTitleList.get(position) ,Jzvd.SCREEN_WINDOW_NORMAL);
            if (autoPlay) {
                videoPlayer.startVideo();
            }
            isRatingDialogShown = false;
            final Handler handler = new Handler();
            Runnable runnable = new Runnable() {

                @Override
                public void run() {
                    try{
                        if (videoPlayer.currentState == 6) {
                            if (!isRatingDialogShown) {
                                if (ratingList.get(position).equals("0.0")) {
                                    showRatingDialog(position);
                                } else {

                                }
                                isRatingDialogShown = true;
                            }
                        }
                    }
                    catch (Exception e) {

                    }
                    finally{
                        if (!isRatingDialogShown) {
                            handler.postDelayed(this, 500);
                        }
                    }
                }
            };

            if (ratingList.get(position).equals("0.0")) {
                handler.post(runnable);
            }  else {

            }
        }
    }

    /**
     * This method handles every onClicks
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_home_fvp:
                setFragment(new HomeFragment());
                break;

            case R.id.no_media_fvp:
                if (mediaPathList.size() != 0) {
                    presenter.doLoadMedia(mediaUrlPath);
                } else {
                    Toast.makeText(mActivity, "No tracks available", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    /**
     * This method is basic onPause method which will handle the minimize events of views
     */
    @Override
    public void onPause() {
        super.onPause();
        Log.e("onPause", "Called");
        if (videoPlayer.isCurrentPlay()) {
            videoPlayer.release();
            videoPlayer.resetProgressAndTime();
        }
    }

    /**
     * This method is basic onResume method which will handle the maximize or starting events of views
     */
    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume", "Called");
        if (goToSettings) {
            presenter.doLoadMedia(mediaUrlPath);
            goToSettings = false;
        }
    }

    /**
     * This method will call when the view is destroyed
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("onDestroy", "Called");
        if (videoPlayer != null) {
            if (videoPlayer.isCurrentPlay()) {
                videoPlayer.release();
            }
        }
    }

    /**
     * This will show the rating dialog
     */
    public void showRatingDialog(int position){
        Log.e("11111111111", "2222222222");

        ScaleRatingBar ratingBar = dialog.findViewById(R.id.simpleRatingBar_cdr);
        ImageView ivClose = dialog.findViewById(R.id.iv_close_cdr);
        TextView tvTrackTitle = dialog.findViewById(R.id.tv_track_title_cdr);
        tvTrackTitle.setTypeface(sansExtraBold);
        tvTrackTitle.setText(mediaTitleList.get(position));
        ratingBar.setOnRatingChangeListener(null);
        ratingBar.setRating(Float.parseFloat(ratingList.get(position)));
        rating[0] = Float.parseFloat(ratingList.get(position));
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                ratingBar.setOnRatingChangeListener(new BaseRatingBar.OnRatingChangeListener() {
                    @Override
                    public void onRatingChange(BaseRatingBar baseRatingBar, float v) {

                        rating[0] = v;
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Do something after 5s = 5000ms

                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            }
                        }, 1000);
                    }
                });
            }
        }, 500);

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                    videoTrackList.get(position).setUserRating(String.valueOf(rating[0]).substring(0,3));

                    Gson gson = new Gson();
                    String modofiedTracks = gson.toJson(videoTrackList);

                    if (comingFrom.equals("Dhyana")) {
                        storageService.storeDhyanaTracks(modofiedTracks);
                    } else {
                        storageService.storePranaTracks(modofiedTracks);
                    }

//                                String currentTrack = storageService.getAudioTracks();
//                                audioTrackList = gson.fromJson(currentTrack, type);

                    ratingList.set(position,String.valueOf(rating[0]).substring(0,3));

                    videoPlayingAdapter.notifyDataSetChanged();
                }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if (!dialog.isShowing()) {
            dialog.show();
        }
    }
    /**
     * This method shows the internet dialog
     */
    void showInternetDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Please connect to internet")
                .setCancelable(true)
                .setPositiveButton("Connect", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                        goToSettings = true;
                    }
                });
//                .setNegativeButton("Quit", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.dismiss();
//                    }
//                });
        AlertDialog alert = builder.create();
        alert.show();

    }
}
