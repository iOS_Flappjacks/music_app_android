package com.musicapp.musicplayer.core.ui.nowPlayingFragment;

import android.media.MediaPlayer;

/**
 * This is the view interface for the NowPlayingFragment.
 *
 * @author Akhil Aravind
 */
public interface NowPlayingFragmentView {

    /**
     * This method will save the tracks to preference.
     *@param position the position of that track
     *@param mediaPath media URL
     *@param cancelFlag whether user cancelled downloading or not
     */
    void saveMediaPath(int position, String mediaPath, Boolean cancelFlag);

    /**
     * This method will show progress dialogs.
     */
    void showProgressDialog();

    /**
     * This method will hide progress dialogs.
     */
    void hideProgressDialog();

    /**
     * This method will show toasts.
     * @param message the message to be toasted
     */
    void showMessage(String message);

    /**
     * This method will load the url
     * @param currentPlaying the url to be loaded
     */
    void loadMedia(String currentPlaying);

    /**
     * This method will play the track
     */
    void doPlayMedia();

    /**
     * This method will pause the track
     */
    void doPauseMedia();

    /**
     * This method will switch to next track
     */
    void doSwitchNext();

    /**
     * This method will switch to next track
     */
    void doSwitchPrevious();

    /**
     * This method will fetch the song details
     * @param mediaPlayer The mediaplayer variable to fetch the song details
     */
    void fetchSongDetails(MediaPlayer mediaPlayer);

}

