package com.musicapp.musicplayer.core.ui.videoPlayFragment;

/**
 * This interface is used to get the click events on the views.
 *
 * @author Akhil Aravind
 */
public interface RecyclerViewClickInterfaceVideoPlay {

    /**
     * This method will handle the click on the parent view
     */
    void recyclerviewOnClick(int position, String mediaUrl, String mediaImage);

    /**
     * This method will handle the click on the download option
     */
    void recyclerviewOnClickDownload(int position, String s);

    /**
     * This method will handle the click on the rate option
     */
    void recyclerviewOnClickRate(int position);
}