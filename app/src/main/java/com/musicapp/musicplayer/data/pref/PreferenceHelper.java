package com.musicapp.musicplayer.data.pref;

public interface PreferenceHelper {

    /**
     * This method stores the Dhwani tracks
     * @param name  the tracks to be saved in json format
     */
    void storeAudioTracks(String name);

    /**
     * This method fetches the Dhwani tracks
     */
    String getAudioTracks();

    /**
     * This method stores the Dhyana tracks
     * @param jsonTracks  the tracks to be saved in json format
     */
    void storeDhyanaTracks(String jsonTracks);

    /**
     * This method fetches the Dhyana tracks
     */
    String getDhyanaTracks();

    /**
     * This method stores the Prana tracks
     * @param jsonTracks  the tracks to be saved in json format
     */
    void storePranaTracks(String jsonTracks);

    /**
     * This method fetches the Prana tracks
     */
    String getPranaTracks();
}